<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'lzrin.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript">
	function dateonblur(){
		//alert("1111111111");
		var date=document.getElementById("dateid").value;
		//alert(date);
		var pattern=/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;
		if(!pattern.test(date)){
			alert("日期输入有误,请满足xxxx-xx-xx的形式");
		}
		return true;
	
	}

</script>
  </head>
  	
  
  <body>
   <form action="servlet/lzr1Servlet" method="get">
   		<table >
   			<tr><td>任务单要求完成日期：</td><td><input type="date" name="datename" id="dateid" value="请满足xxxx-xx-xx格式" onBlur="dateonblur()"></td></tr>
   			<tr><td>任务类型：</td><td><select name="typename">
   									<option value="所有类型">所有类型</option>
   									<option value="收款">收款</option>
   									<option value="送货收款">送货收款</option>
   									<option value="送货">送货</option>
   									<option value="退货">退货</option>
   									<option value="换货">换货</option></select></td></tr>
   			<tr><td>任务状态：</td><td><select name="statename" >
   									<option value="所有状态">所有状态</option>
   									<option value="可调度">可调度</option>
    	                      		<option value="可分配">可分配</option>
    	                      		<option value="已分配">已分配</option>
    	                      		<option value="已领货">已领货</option>
    	                      		<option value="完成">完成</option>
    	                      		<option value="失败">失败</option></select></td></tr>
   		</table>
   		
    	<input type="submit" value="任务单查询">
    </form>
  </body>
</html>
