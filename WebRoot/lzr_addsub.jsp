<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="renderer" content="webkit" />
	<link href="css/bootstrap.min.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
	<link href="css/animate.css" rel="stylesheet" />
	<link href="css/style.css" rel="stylesheet" />
	<link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">
</head>
<body class="top-navigation">
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" data-role="navigation">
<!-- 以下是顶部导航栏，需要根据页面配置 -->
					<div class="navbar-header">
						<div class="minimalize-styl-2">
						
<!-- 请确保下面的<a>标签中的href属性地址和相对路径一致 -->
							<a class="btn btn-success" title="返回主页" href="main.html"><i class="fa fa-lg fa-home"></i></a>
<!-- 请确保上面的<a>标签中的href属性地址和相对路径一致 -->

							<a class="back-link btn btn-success" title="后退" href="main.html"><i class="fa fa-lg fa-arrow-left"></i></a>
							<a class="reload-link btn btn-success" title="重新加载"><i class="fa fa-lg fa-repeat"></i></a>

<!-- 以下是指示页面上下文的部分，需要根据实际路径修改 -->
							<ol id="guide" class="breadcrumb m-l">
<!-- <li><a>上级页面的上级页面</a><li> -->
<!-- <li><a>上级页面</a><li> -->
<!-- <li><strong>当前页面</strong><li> -->
								<li><strong>主页</strong></li>
							</ol>
<!-- 以上是指示页面上下文的部分，需要根据实际路径修改 -->

						</div>
					</div>
					<div class="nav navbar-top-links navbar-right minimalize-styl-2">
						<span class="m-r-sm text-muted welcome-message btn btn-link">欢迎使用Feces物流管理系统</span>
					</div>
<!-- 以上是顶部导航栏，需要根据页面配置 -->
				</nav>
			</div>
			
			
			<!-- 配置点2 -->
			<!-- 内容 -->
			<div class="wrapper wrapper-content animated fadeInRight">
			
			
				<!-- 配置点2.1 -->
				<!-- 段落1 -->
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>添加分站</h5>
								<div class="ibox-tools">
									<a class="collapse-link btn-link">
										收起/展开<i class="fa fa-chevron-up"></i>
									</a>
								</div>
							</div>
							<div class="ibox-content">
								<form action="servlet/lzr12Servlet" method="get">
								<table class="table table-striped table-bordered table-hover " id="editable">
								<thead>
									<tr>
										<th>添加要求</th>
										<th>输入</th>
										
									</tr>
								</thead>
								<tbody>
									<tr class="gradeA">
										<td>分站号：</td>
										<td><input type="text" name="subid" ></td>
										
									</tr>
									<tr class="gradeX">
										<td>分站名：</td>
										<td><input type="text" name="subname"></td>
									
										
									</tr>
									<tr class="gradeC">
										<td>库房编号：</td>
										<td><input type="text" name="storeid"></td>  	                      
    	                      		
    	                      		
    	                      	
										
									</tr>
								
								</tbody>
								<tfoot>
									<tr class="gradeU">
									<td>请点击</td>
										<td>
										<input type="submit" value="添加" class="btn btn-primary ">
										
									
									
									</td>
								
									
									</tr>
								
								</tfoot>
							</table>
							</form>
								
								
								
								
								
							</div>
						</div>
					</div>
				</div>
				
				
				<!-- 配置点2.2 -->
				<!-- 段落2 -->
			
				
				<!-- 配置点2.x -->
				<!-- ... -->
				
				
			</div>
		</div>
	</div>
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="js/plugins/toastr/toastr.min.js"></script>
	<script src="js/plugins/pace/pace.min.js"></script>
	<script src="js/feces.js"></script>
	
	
	<!-- 配置点3 -->
	<!-- 载入后的js脚本 -->
	<script>
$(function(){
	// you JavaScript code here!
});

function dateonblur(){
		//alert("1111111111");
		var date=document.getElementById("dateid").value;
		//alert(date);
		var pattern=/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;
		if(!pattern.test(date)){
			alert("日期输入有误,请满足xxxx-xx-xx的形式");
		}
		return true;
	
	}
	</script>
	
	
</html>


