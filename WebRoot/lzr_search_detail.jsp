<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ page import ="feces.model.TaskVoucher" %>
<%@ page import ="feces.model.ClientOrder" %>
<%@ page import ="feces.model.ClientOrderList" %>
<%@ page import ="feces.model.Client" %>
<%@ page import ="feces.model.Product" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="renderer" content="webkit" />
	<link href="css/bootstrap.min.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
	<link href="css/animate.css" rel="stylesheet" />
	<link href="css/style.css" rel="stylesheet" />
	<link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">
</head>
<body class="top-navigation">
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" data-role="navigation">
<!-- 以下是顶部导航栏，需要根据页面配置 -->
					<div class="navbar-header">
						<div class="minimalize-styl-2">
						
<!-- 请确保下面的<a>标签中的href属性地址和相对路径一致 -->
							<a class="btn btn-success" title="返回主页" href="main.html"><i class="fa fa-lg fa-home"></i></a>
<!-- 请确保上面的<a>标签中的href属性地址和相对路径一致 -->

							<a class="back-link btn btn-success" title="后退" href="lzr_search_result.jsp"><i class="fa fa-lg fa-arrow-left"></i></a>
							<a class="reload-link btn btn-success" title="重新加载"><i class="fa fa-lg fa-repeat"></i></a>

<!-- 以下是指示页面上下文的部分，需要根据实际路径修改 -->
							<ol id="guide" class="breadcrumb m-l">
<!-- <li><a>上级页面的上级页面</a><li> -->
<!-- <li><a>上级页面</a><li> -->
<!-- <li><strong>当前页面</strong><li> -->
								<li><strong>主页</strong></li>
							</ol>
<!-- 以上是指示页面上下文的部分，需要根据实际路径修改 -->

						</div>
					</div>
					<div class="nav navbar-top-links navbar-right minimalize-styl-2">
						<span class="m-r-sm text-muted welcome-message btn btn-link">欢迎使用Feces物流管理系统</span>
					</div>
<!-- 以上是顶部导航栏，需要根据页面配置 -->
				</nav>
			</div>
			
			
			
			
			<!-- 配置点2 -->
			<!-- 内容 -->
			<div class="wrapper wrapper-content animated fadeInRight">
			
			
			
			
				<!-- 配置点2.1 -->
				<!-- 段落1 -->
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>查看任务单具体信息</h5>
							</div>
							<div class="ibox-content">
								<table class="table table-striped table-bordered table-hover "
								id="editable">
								<thead>
									
								</thead>
								<tbody>
									<%if(session.getAttribute("attr2")!=null) {%>
									<%int i=Integer.parseInt(String.valueOf(session.getAttribute("attr2")));%>
									<%List<TaskVoucher> a=(List<TaskVoucher>)session.getAttribute("attr1") ;%>
									<%TaskVoucher ta=a.get(i); %>
									<tr>
										<th colspan=2>任务号:</th>
										<th colspan=2>${ta.task_number}<%=ta.getTask_number() %></th>
									</tr>
									<tr>
										<th>客户姓名:</th>
										<th>${ta.order.order_client.name}<%=ta.getOrder().getOrder_client().getName()%></th>
										<th>客户地址:</th>
										<th>${ta.order.order_client.address}<%=ta.getOrder().getOrder_client().getAddress()%>
										</th>
									</tr>
									
										<%List<ClientOrderList> pa=ta.getOrder().getProducts(); %>
										<%for(ClientOrderList p:pa){ %>
										<tr>
											<th>商品名称:</th>
											<th>${p.product.name}<%=p.getProduct().getName()%></th>
											<th>商品数目:</th>
											<th>${p.product.product_number}<%=p.getProduct_number()%></th>
										</tr>
										<% }%>
									
									
									<tr>
										<th colspan=2>要求完成日期:</th>
										<th colspan=2>${ta.date}<%=ta.getDate() %></th>
									</tr>
									<tr>
										<th colspan=2>任务类型:</th>
										<th colspan=2>${ta.task_type}<%=ta.getTask_type() %></th>
									</tr>
									<tr>
										<th colspan=2>任务状态:</th>
										<th colspan=2>${ta.task_state}<%=ta.getTask_state() %></th>
									</tr>
									<%if(ta.getTask_state().equals("失败")){ %>
									<tr>
										<form action="servlet/lzr11Servlet" action="get">
											<th colspan=4>
											<center>
											<input type="hidden" value="<%=ta.getTask_number()%>" name="hide_id">
												<input type="submit" value="退货" class="btn btn-primary ">
											</center>
											
											</th>
										</form>
									</tr>
									<%} %>
									
									<%} %>
								</tbody>
								<tfoot>
								
								</tfoot>
							</table>
								
								
								
								
								
								
							</div>
						</div>
					</div>
				</div>
				
				
				
				
				<!-- 配置点2.2 -->
				<!-- 段落2 -->
					
				
				
				<!-- 配置点2.x -->
				<!-- ... -->
				
				
				
				
			</div>
		</div>
	</div>
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="js/hplus.js"></script>
	<script src="js/plugins/pace/pace.min.js"></script>
	<script src="js/plugins/toastr/toastr.min.js"></script>
	
	
	
	
	<!-- 配置点3 -->
	<!-- 载入后的js脚本 -->
	<script>
$(function(){
	// you JavaScript code here!
});
	</script>
	
	
	
	
</html>


