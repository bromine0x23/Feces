$(function() {

	// MetsiMenu
	$('#side-menu').metisMenu();

	// Collapse ibox function
	$('.collapse-link').click(function() {
		var link = $(this);
		var ibox = link.closest('div.ibox');
		var button = link.find('i');
		button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
		ibox.toggleClass('').toggleClass('border-bottom');
		ibox.find('div.ibox-content').slideToggle(200, function(){
			ibox.resize();
			ibox.find('[id^=map-]').resize();
		});
	});

	// Close ibox function
	$('.close-link').click(function() {
		$(this).closest('div.ibox').remove();
	});
	
	$('.back-link').click(function() {
		window.history.back();
	});
	
	$('.reload-link').click(function() {
		window.location.reload();
	});

	// Small todo handler
	$('.check-link').click(function() {
		var link = $(this);
		link.find('i').toggleClass('fa-check-square').toggleClass('fa-square-o');
		link.next('span').toggleClass('todo-completed');
		return false;
	});

	// Move modal to body
	// Fix Bootstrap backdrop issu with animation.css
	$('.modal').appendTo('body');

	$('[data-toggle=popover]').popover();
	
	// Minimalize menu when screen is less than 768px
	$(window).bind("load resize", function() {
		if ($(this).width() < 769) {
			$('body').addClass('body-small');
		} else {
			$('body').removeClass('body-small');
		}
	});
	
	if (window.frameElement) {
		$('#wrapper').resize(function(){
			var height = $(this).height();
			$(window.frameElement).height(height).parent().height(height);
		});
	}
	
	if ($.validator) {
		$.validator.setDefaults({
		    highlight: function (element) {
		        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		    },
		    success: function (element) {
		        element.closest('.form-group').removeClass('has-error').addClass('has-success');
		    },
		    errorElement: "span",
		    errorClass: "help-block m-b-none",
		    validClass: "help-block m-b-none"
		});
	}
	//设置toastr参数
	if (toastr) {
		if (frameElement) {
			toastr = parent.toastr;
		} else {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"progressBar": true,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "400",
				"hideDuration": "1000",
				"timeOut": "7000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			};
		}
	}
	if ($.fn.select2) {
		$.fn.select2.defaults.set('width', 'style');
	}
	// 设置x-editable
	if ($.fn.editableform) {
		$.fn.editableform.template =
'<form class="form-inline editableform">' + 
	'<div class="control-group">' + 
		'<div>' + 
			'<div class="form-group editable-input"></div>' +
			'<div class="form-group editable-buttons"></div>' +
		'</div>' +
		'<div class="editable-error-block"></div>' +
	'</div>' +
'</form>';
		$.fn.editableform.buttons = 
'<button type="submit" class="btn btn-primary btn-outline btn-sm editable-submit">' +
	'<em class="fa fa-check"></em>' +
'</button>' +
'<button type="button" class="btn btn-default btn-outline btn-sm editable-cancel">' +
	'<em class="fa fa-times"></em>' +
'</button>';
	}
});
window.Feces = {
	url: {
		products: 'api/products',
		product: 'api/product',
		product_major_types: 'api/product_major_types',
		product_major_type: 'api/product_major_type',
		product_minor_types: 'api/product_minor_types',
		product_minor_type: 'api/product_minor_type',
		storehouses: 'api/storehouses',
		storehouse: 'api/storehouse',
		suppliers: 'api/suppliers',
		supplier: 'api/supplier',
		storehouse_products: 'api/storehouse_products',
		storehouse_product: 'api/storehouse_product',
		store_records: 'api/store_records'
	}
};