$(function(){
	var pair = document.URL.split('/').pop().split('?');
	if (pair.length < 2) {
		return;
	}
	var parameters = pair[1].split('&');
	var id = function(parameters){
		for (var i = 0; i < parameters.length; ++i) {
			var pair = parameters[i].split('=');
			if (pair[0] === 'id') {
				return pair[1];
			}
		}
	}(parameters);
	if (!id) {
		return;
	}
	var $major_types = null;
	var $minor_types = null;
	var $minor_types_by_major = {};
	var task0 = $.get('../' + Feces.url.product_major_types + '.json').done(function(result){
		$major_types = result.data.sort(function(a, b){ return a.id - b.id; });
		$.each($major_types, function(){
			this.text = this.name;
		});
	});
	var task1 = $.get('../' + Feces.url.product_minor_types + '.json').done(function(result){
		$minor_types = result.data.sort(function(a, b){ return a.id - b.id; });
		$.each($minor_types, function(){
			this.text = this.name;
			var major_id = this.majorType.id;
			var minor_types = $minor_types_by_major[major_id];
			if (minor_types) {
				minor_types.push(this);
			} else {
				$minor_types_by_major[major_id] = [this];
			}
		});
	});
	var task2 = $.get('../' + Feces.url.suppliers + '.json').done(function(result){
		$suppliers = result.data.sort(function(a, b){ return a.id - b.id; });
		$.each($suppliers, function(){
			this.text = this.name;
		});
	});
	$.when(task0, task1, task2).done(function(){
		$.get('../' + Feces.url.product + '.json', { id: id }).done(function(response){
			var product = response.data;
			$('#display-name').empty().text(product.name).data({
				name: 'name',
				type: 'text',
				title: '商品名',
				placeholder: '请输入商品名'
			}).attr('data-editable', true);
			$('#display-minor').empty().text(product.minorType.majorType.name + ' / ' + product.minorType.name).data({
				name: 'minor_id',
				type: 'product_type',
				title: '商品分类',
				value: product.minorType.id,
				placeholder: '请选择商品分类',
				major_types: $major_types,
				minor_types: $minor_types,
				minor_types_by_major: $minor_types_by_major
			}).attr('data-editable', true);
			$('#display-unit').empty().text(product.unit).data({
				name: 'unit',
				type: 'text',
				title: '计量单位',
				placeholder: '请输入计量单位',
			}).attr('data-editable', true);
			$('#display-price').empty().text(product.price).data({
				name: 'price',
				type: 'number',
				title: '原价',
				min: 0,
				step: 0.01,
				placeholder: '请输入原价'
			}).attr('data-editable', true);
			$('#display-discount').empty().text(product.discount).data({
				name: 'discount',
				type: 'number',
				title: '折扣',
				min: 0,
				max: 100,
				step: 0.01,
				placeholder: '请输入折扣',
				display: function(value) {
					$(this).text(value + '%');
				}
			}).attr('data-editable', true);
			$('#display-cost').empty().text(product.cost).data({
				name: 'cost',
				type: 'number',
				title: '成本',
				min: 0,
				step: 0.01,
				placeholder: '请输入成本',
			}).attr('data-editable', true);
			$('#display-type').empty().text(product.type).data({
				name: 'type',
				type: 'text',
				title: '型号',
				placeholder: '请输入型号',
				validate: null
			}).attr('data-editable', true);
			$('#display-supplier').empty().text(product.supplier.name).data({
				name: 'supplier_id',
				type: 'select2',
				title: '供应商',
				value: product.supplier.id,
				source: $suppliers
			}).attr('data-editable', true);
			$('#display-productor').empty().text(product.productor).data({
				name: 'productor',
				type: 'text',
				title: '厂商',
				placeholder: '请输入厂商',
			}).attr('data-editable', true);
			$('#display-period').empty().text(product.period).data({
				name: 'period',
				type: 'date',
				title: '保质期限',
				placeholder: '请设置保质期限',
				placement: 'left'
			}).attr('data-editable', true);
			$('#display-returnable').empty().text(product.returnable);
			$('#display-changable').empty().text(product.changable);
			$('#display-remark').empty().text(product.remark).data({
				name: 'remark',
				type: 'textarea',
				title: '备注',
				placeholder: '请输入备注',
				validate: function(value){
					if($.trim(value).length > 200) {
						return '备注过长';
					}
				}
			}).attr('data-editable', true);
			$('[data-editable=true]').each(function(){
				var cell = $(this);
				cell.wrapInner($('<a>')).children().editable($.extend({
					pk: id,
					emptytext: '无',
					ajaxOptions: { type: 'PUT' },
					url: '../' + Feces.url.product + '.json',
					success: function(response, new_value){
						if (response.status !== 0) {
							return response.message;
						}
					},
					validate: function(value){
						if($.trim(value) === '') {
							return '不能为空';
						}
					}
				}, cell.data()));
			});
		});
	});
});