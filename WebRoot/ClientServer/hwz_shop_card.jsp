<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ page import ="java.util.ArrayList" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="renderer" content="webkit" />
	<link href="../css/bootstrap.min.css" rel="stylesheet" />
	<link href="../css/font-awesome.min.css" rel="stylesheet" />
	<link href="../css/animate.css" rel="stylesheet" />
	<link href="../css/style.css" rel="stylesheet" />
	<link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">
	<!-- Data Tables -->
	<link href="../css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
	<link href="../css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	<link href="../css/plugins/steps/jquery.steps.css" rel="stylesheet">
	<link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">
</head>
<body class="top-navigation">
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
<nav class="navbar navbar-static-top" data-role="navigation">
<!-- 以下是顶部导航栏，需要根据页面配置 -->
					<div class="navbar-header">
						<div class="minimalize-styl-2">
						
<!-- 请确保下面的<a>标签中的href属性地址和相对路径一致 -->
							<a class="btn btn-success" title="返回主页" href="main.html"><i class="fa fa-lg fa-home"></i></a>
<!-- 请确保上面的<a>标签中的href属性地址和相对路径一致 -->

							<a class="back-link btn btn-success" title="后退"><i class="fa fa-lg fa-arrow-left"></i></a>
							<a class="reload-link btn btn-success" title="重新加载"><i class="fa fa-lg fa-repeat"></i></a>

<!-- 以下是指示页面上下文的部分，需要根据实际路径修改 -->
							<ol id="guide" class="breadcrumb m-l">
<!-- <li><a>上级页面的上级页面</a><li> -->
<!-- <li><a>上级页面</a><li> -->
<!-- <li><strong>当前页面</strong><li> -->
								<li><strong>主页</strong></li>
							</ol>
<!-- 以上是指示页面上下文的部分，需要根据实际路径修改 -->

						</div>
					</div>
					<div class="nav navbar-top-links navbar-right minimalize-styl-2">
						<span class="m-r-sm text-muted welcome-message btn btn-link">欢迎使用Feces物流管理系统</span>
					</div>
<!-- 以上是顶部导航栏，需要根据页面配置 -->
				</nav>
			</div>
			
			
			<!-- 配置点2 -->
			<!-- 内容 -->
			<button type="button" onclick="tr_sub()" class="btn btn-success">确认订单</button>
			<input type="text" id="order_id">
			<div class="wrapper wrapper-content animated fadeInRight">
					<table id="editable" class="table table-striped table-bordered table-hover ">
						<thead>
							<tr>
								<td>商品名称</td>
								<td>商品单价</td>
								<td>库存</td>
								<td>数量</td>
								<td>合计</td>
								<td>操作</td>
							</tr>
						</thead>
					</table>
				<!-- 配置点2.2 -->
				<!-- 段落2 -->
						<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>订单信息录入</h5>
								<div class="ibox-tools">
									<a class="collapse-link btn-link">
										收起/展开<i class="fa fa-chevron-up"></i>
									</a>
								</div>
							</div>
							
							<div class="ibox-content">
								<form class="m-t" data-role="form">
											<table>
											<tr class="form-group">
												<td>
													<label class="label label-info">送货日期</label>
												</td>
												<td>
													<div class="form-group" id="data_1">
														<div class="input-group date">
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
															<input type="text" id="send_date" class="form-control" value="2014-11-11">
														</div>
													</div>
												</td>
												<td></td>
											</tr>
											<tr class="form-group">
											<td>
												<label class="label label-info">订单类型</label>
											</td>
											<td>
													<select class="form-control m-b" id="in_order_type">
														<option>新订</option>
														<option>新订异地</option>
													</select>
											</td>
											<td></td>
											</tr>
											<tr class="form-group">
												<td>
													<label class="label label-info">备注</label>
												</td>
												<td>
													<input type="text" class="form-control" id="in_rem" placeholder="请输入备注" required="">
												</td>
												<td><p id="remark_part"></p></td>
											</tr>
											<tr class="form-group">
												<td>
													<label class="label label-info">商品说明</label>
												</td>
												<td>
												<input type="text" class="form-control" id="in_pro_rem" placeholder="请输入商品说明" required="">
												</td>
												
											</tr>
											</table>

										</form>

									</div>
								</div>
							</div>
						</div>
				<!-- 配置点2.3 -->
				<!-- 段落3 -->
						<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>收货信息录入</h5>
								<div class="ibox-tools">
									<a class="collapse-link btn-link">
										收起/展开<i class="fa fa-chevron-up"></i>
									</a>
								</div>
							</div>
							<div class="ibox-content">
								<form class="m-t" data-role="form">
								<table>
											<tr>
												<td>
													<label class="label label-info">送货地址</label>
												</td>
												<td>
													<input type="text" class="form-control" id="in_destination" placeholder="请输入送货地址" required="">
												</td>
											</tr>
											<tr>
												<td>
													<label class="label label-info">接收人</label>
												</td>
												<td>
													<select class="form-control m-b" id="account">
														<option>Mary</option>
														<option>Jack</option>
														<option>Alex</option>
														<option>Mark</option>
													</select>
												</td>
											</tr>
											<tr>
												<td>
													<label class="label label-info">电话</label>
												</td>
												<td>
													<input type="text" class="form-control" id="in_phone" placeholder="请输入电话" required="">
												</td>
											</tr>
											<tr>
												<td>
													<label class="label label-info">邮编</label>
												</td>
												<td>
													<input type="text" class="form-control" id="in_mail" placeholder="请输入邮编" required="">
												</td>
											</tr>
											<tr>
												<td>
													<label class="label label-info">是否要发票</label>
												</td>
												<td>
													<select class="form-control m-b" id="check_need">
														<option>yes</option>
														<option>no</option>
													</select>
												</td>
											</tr>
										</table>
								</form>
									</div>
								</div>
							</div>
						</div>
				<div class="add">				
					<button id="add_order" class="btn btn-outline btn-success">添加</button>
				</div>



				<!-- 配置点2.x -->
				<!-- ... -->
				
				
				
			</div>
		</div>
	</div>
	<script src="../js/jquery-2.1.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="../js/plugins/toastr/toastr.min.js"></script>
	<script src="../js/feces.js"></script>
	<!-- Data Tables -->
	<script src="../js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="../js/plugins/dataTables/dataTables.bootstrap.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="../js/plugins/pace/pace.min.js"></script>
	    <!-- Data picker -->
    <script src="../js/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- Jquery Validate -->
    <script src="../js/plugins/validate/jquery.validate.min.js"></script>

    <script src="../js/plugins/validate/messages_zh.min.js"></script>
    
    <script src="../js/plugins/staps/jquery.steps.min.js"></script>
    
    
    <!-- Toastr script -->
    <script src="../js/plugins/toastr/toastr.min.js"></script>
    	<!-- 配置点3 -->
	<!-- 载入后的js脚本 -->
	<script>

	$(document).ready(function()
			{
				$('#data_1 .input-group.date').datepicker({
				todayBtn: "linked",
				keyboardNavigation: false,
				forceParse: false,
				calendarWeeks: true,
				autoclose: true
				});
						
			});
$(function(){
});
<%String id=request.getParameter("id");%>
var orc=<%=id%>;
var gcl=<%=id%>;
var prolist= new Array();
var left_pro=new Array();
var left_count=[];
var sum;
var index=0;
$(document).ready(function()
{
	var url="../api/pro";
	var list="<%=request.getParameter("list")%>";
	prolist=list.split(","); //字符分割 
	var lg=prolist.length;

	for(var i=0;i<lg;i++)
	{
		$.ajax({
			type: "GET",
			url: url,
			data: {
				id:prolist[i]
			},
			success: function(result)
			{
				pro=result.data.product;
				sum=result.data.sum;
				var x={"product_id":pro.id,"product_number":1};
				$('#editable').dataTable()
				.fnAddData(
					[
					pro.name,
					pro.price,
					sum,
					"<input type='text' onkeyup='calculate("
							+pro.price
							+","
							+pro.id
							+","
							+i
							+")'class='sub-info'"
							+"id='num"
							+pro.id
							+"'value='1'>",

					"<a  id='cal"+pro.id+"'></a>",
					"<a onclick='del_pro("+pro.id+")' id='pro"
					+pro.id
					+"'>删除</a>"
					]);
				left_pro[index++]=x;
			}
		});
	}
});	

$(document).ready(function() {
	$('.dataTables-example').dataTable();

	/* Init DataTables */
	var oTable = $('#editable').dataTable();

	/* Apply the jEditable handlers to the table */
	oTable.$('td').editable('../example_ajax.php', {
		"callback" : function(sValue, y) {
			var aPos = oTable.fnGetPosition(this);
			oTable.fnUpdate(sValue, aPos[0], aPos[1]);
		},
		"submitdata" : function(value, settings) {
			return {
				"row_id" : this.parentNode.getAttribute('id'),
				"column" : oTable.fnGetPosition(this)[2]
			};
		},

		"width" : "90%",
		"height" : "100%"
	});

});
var flag=1;
function calculate( price ,id,i)
{
	var num=$("#num"+id).val();
	if(num > sum)
	{
		toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-center",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
		};
		toastr.error("您订购的产品量："+num+",超过库存总额:"+sum+"！","库存不够") ;
		flag=0;
	}
	else
	{
		flag=1;
	}
	$("#cal"+id).text(num*price);
	left_pro[index].product_number=num;
}
function tr_sub()
{ 
	var type="新订";
	var tyb=$("#in_order_type");
	if(tyb.val()!=null)
	{
		type=tyb.val();
	}
	else
	{
		typ.append("<a>订单类型不能为空</a>");
	}
	var remark;
	var rem=$("#in_rem");
	if((rem.val()!=null)&&(rem.val()!=""))
	{
		alert(rem.val());
		remark=rem.val();
	}
	else
	{
		alert("备注不能为空");
		$("#remark_part").append("<font size=\"3\" face=\"arial\" color=\"red\">* 备注不能为空</font>");
		return ;
	}
	var send_info;
	var rem=$("#in_pro_rem");
	if((rem.val()!=null)&&(rem.val()!=""))
	{
		alert(rem.val());
		send_info=rem.val();
	}
	else
	{
		send_info="无";
		return ;
	}
	var url1="../api/order";
	if(flag==1)
	{
		$.ajax({
			type: "POST",
			url: url1,
			data: {
				"pro_list":JSON.stringify(left_pro),
				order_client:orc,
				get_client:gcl,
				order_date:$("#send_date").val(),
				type:type,
				remark:remark,
				send_info:send_info,
				ch_info:$("#check_need").val()
			},

			success: function(result)
			{
				toastr.options = {
						  "closeButton": true,
						  "debug": false,
						  "progressBar": true,
						  "positionClass": "toast-top-center",
						  "onclick": null,
						  "showDuration": "400",
						  "hideDuration": "1000",
						  "timeOut": "7000",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "fadeIn",
						  "hideMethod": "fadeOut"
						};
				toastr.success("订单已进入调度中心数据库", "订单添加成功");
			}
		});
	}
	else
	{
		$.ajax({
			type: "POST",
			url: url1,
			data: {
				"pro_list":JSON.stringify(left_pro),
				order_client:orc,
				get_client:gcl,
				order_date:$("#send_date").val(),
				type:"缺货",
				remark:remark,
				send_info:"缺货",
				ch_info:"缺货"
			},

			success: function(result)
			{
				toastr.options = {
						  "closeButton": true,
						  "debug": false,
						  "progressBar": true,
						  "positionClass": "toast-top-center",
						  "onclick": null,
						  "showDuration": "400",
						  "hideDuration": "1000",
						  "timeOut": "7000",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "fadeIn",
						  "hideMethod": "fadeOut"
						};
				toastr.success("缺货单单已进入调度中心数据库", "缺货单成功");
			}
		});
	}
}

//删除行，并删除指定行
function del_pro(ckb)
{
	alert(ckb);
	var lg=left_pro.length;
	for(var i=0;i<lg;i++)
	{
		//alert("计数："+i+",id="+left_pro[i].product_id+",ckb="+ckb);
		if(left_pro[i].product_id==ckb)
		{
			$("#pro"+ckb).parent().parent().remove();
			index--;
			left_pro.splice(i,1);
		}
	}
}
</script>	
</html>

