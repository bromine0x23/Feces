<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="zh-CN">
<head>
 <base href="<%=basePath%>">
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="renderer" content="webkit" />
	<link href="css/bootstrap.min.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
	<link href="css/animate.css" rel="stylesheet" />
	<link href="css/style.css" rel="stylesheet" />
	<link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">
	<!-- Data Tables -->
	<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
</head>
<body class="top-navigation">
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
<nav class="navbar navbar-static-top" data-role="navigation">
<!-- 以下是顶部导航栏，需要根据页面配置 -->
					<div class="navbar-header">
						<div class="minimalize-styl-2">
						
<!-- 请确保下面的<a>标签中的href属性地址和相对路径一致 -->
							<a class="btn btn-success" title="返回主页" href="main.html"><i class="fa fa-lg fa-home"></i></a>
<!-- 请确保上面的<a>标签中的href属性地址和相对路径一致 -->

							<a class="back-link btn btn-success" title="后退"><i class="fa fa-lg fa-arrow-left"></i></a>
							<a class="reload-link btn btn-success" title="重新加载"><i class="fa fa-lg fa-repeat"></i></a>

<!-- 以下是指示页面上下文的部分，需要根据实际路径修改 -->
							<ol id="guide" class="breadcrumb m-l">
<!-- <li><a>上级页面的上级页面</a><li> -->
<!-- <li><a>上级页面</a><li> -->
<!-- <li><strong>当前页面</strong><li> -->
								<li><strong>主页</strong></li>
							</ol>
<!-- 以上是指示页面上下文的部分，需要根据实际路径修改 -->

						</div>
					</div>
					<div class="nav navbar-top-links navbar-right minimalize-styl-2">
						<span class="m-r-sm text-muted welcome-message btn btn-link">欢迎使用Feces物流管理系统</span>
					</div>
<!-- 以上是顶部导航栏，需要根据页面配置 -->
				</nav>
			</div>
			
			
			<!-- 配置点2 -->
			<!-- 内容 -->
			<div class="wrapper wrapper-content animated fadeInRight">
			
			
				<!-- 配置点2.1 -->
				<!-- 段落1 -->
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>客户信息</h5>
								<div class="ibox-tools">
									<a class="collapse-link btn-link">
										收起/展开<i class="fa fa-chevron-up"></i>
									</a>
								</div>
							</div>
							<div class="ibox-content">
							<form action="api/excpro">
							
							<table class="table table-striped table-bordered table-hover " id="editable">
								<thead>
									<tr>
										<th>订单号</th>
										<th>商品名称</th>
										<th>单价</th>
										<th>退订数量</th>
										<th>订单状态</th>
										<th>订单日期</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
							<div class="row">
								<div class="col-sm-6">
									<div class="col-md-12">
										<div class="form-group">
											<label class="col-sm-3 control-label">退换货原因：</label>
											<div class="col-sm-9">
												<select class="form-control" name="back_info">
													<option>商品破损</option>
													<option>无法满足要求</option>
													<option>七天之内无条件退换货</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="col-md-12">
										<div class="form-group">
											<label class="col-sm-3 control-label">退/换货：</label>
											<div class="col-sm-9">
												<select class="form-control" name="info">
													<option>退货</option>
													<option>换货</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							<span id="test1" style="display:none">
								<input type="text" id="uname" name="id" />
							</span>
							<div  class="col-sm-6" >
							<button type="submit"  class="btn-success">确认退、换货</button></br>
							<p id="date"><small>日期：</small></p>
							</div>
							</div>
							</form>
							</div>
						</div>
					</div>
				</div>
				
				
				<!-- 配置点2.x -->
				<!-- ... -->
				
				
			</div>
		</div>
	</div>
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="js/plugins/toastr/toastr.min.js"></script>
	<script src="js/plugins/pace/pace.min.js"></script>
	<script src="js/feces.js"></script>
		<!-- Data Tables -->
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	
	<!-- 配置点3 -->
	<!-- 载入后的js脚本 -->
	<script>
$(function(){
	// you JavaScript code here!
});

$(document).ready(function()
{
	url="api/order";
		$.ajax({
			type: "GET",
			url: url,
			data: {
				id: <%=request.getParameter("id")%>,
			},
			success: function(result){
			var rs = result.data;
			var cll=rs;
			var lg=cll.length;
			for(var i=0;i<lg;i++)
			{
				var cl=cll[i];
				var pd=cl.products;
				var plg=pd.length;
				if(plg==0)
				{
					$('#editable').dataTable()
					.fnAddData(
					[
					  cl.id,
					 cl.num,
					  "null",
					  "null",
					  "null",
					  cl.type]);
				}
				else
				{
					for(var j=0;j<plg;j++)
					{
					$('#editable').dataTable()
						.fnAddData(
						[
						cl.id,
						pd[j].product.name,
						pd[j].product.price,
						pd[j].product_number,
						cl.status,
						cl.created,
						]);
					}
				}
				$("#uname").val(cl.id);
			}
		}
		});

});



$(document).ready(function() {
	var now=new Date();
	var day=now.getDate();
	var year=now.getFullYear();
	//var d=day.toString();
	var month=now.getMonth();
	var txt3=document.createElement("day");  // 通过 DOM 创建元素
	txt3.innerHTML=year+"年"+month+"月"+day+"日";
	$("#date").append(txt3);

});

		$(document).ready(function() {
			$('.dataTables-example').dataTable();

			/* Init DataTables */
			var oTable = $('#editable').dataTable();

			/* Apply the jEditable handlers to the table */
			oTable.$('td').editable('../example_ajax.php', {
				"callback" : function(sValue, y) {
					var aPos = oTable.fnGetPosition(this);
					oTable.fnUpdate(sValue, aPos[0], aPos[1]);
				},
				"submitdata" : function(value, settings) {
					return {
						"row_id" : this.parentNode.getAttribute('id'),
						"column" : oTable.fnGetPosition(this)[2]
					};
				},

				"width" : "90%",
				"height" : "100%"
			});

		});

		function fnClickAddRow() {

			$('#editable').dataTable()
					.fnAddData(
							[ "Custom row", "New row", "New row", "New row",
									"New row" ]);

		}


	</script>
	
	
</html>
