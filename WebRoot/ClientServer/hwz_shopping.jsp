<%@page import="feces.Database"%>
<%@page import="feces.servlet.ProductsServlet"%>
<%@page import="javax.swing.text.PlainDocument"%>
<%@page import="feces.model.Product"%>
<%@page import="feces.servlet.RESTfulServlet"%>
<%@page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">

<title>素材火www.sucaihuo.com - 数据表格</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/bootstrap.min.css?v=3.4.0" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">

<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<link href="css/animate.css" rel="stylesheet">
<link href="css/style.css?v=2.2.0" rel="stylesheet">
</head>
<body class="top-navigation">
<% String id=request.getParameter("cli_id");
%>
	<div class="gray-bg dashbard-1">
<div class="row border-bottom">
<nav class="navbar navbar-static-top" data-role="navigation">
<!-- 以下是顶部导航栏，需要根据页面配置 -->
					<div class="navbar-header">
						<div class="minimalize-styl-2">
						
<!-- 请确保下面的<a>标签中的href属性地址和相对路径一致 -->
							<a class="btn btn-success" title="返回主页" href="main.html"><i class="fa fa-lg fa-home"></i></a>
<!-- 请确保上面的<a>标签中的href属性地址和相对路径一致 -->

							<a class="back-link btn btn-success" title="后退"><i class="fa fa-lg fa-arrow-left"></i></a>
							<a class="reload-link btn btn-success" title="重新加载"><i class="fa fa-lg fa-repeat"></i></a>

<!-- 以下是指示页面上下文的部分，需要根据实际路径修改 -->
							<ol id="guide" class="breadcrumb m-l">
<!-- <li><a>上级页面的上级页面</a><li> -->
<!-- <li><a>上级页面</a><li> -->
<!-- <li><strong>当前页面</strong><li> -->
								<li><strong>主页</strong></li>
							</ol>
<!-- 以上是指示页面上下文的部分，需要根据实际路径修改 -->

						</div>
					</div>
					<div class="nav navbar-top-links navbar-right minimalize-styl-2">
						<span class="m-r-sm text-muted welcome-message btn btn-link">欢迎使用Feces物流管理系统</span>
					</div>
<!-- 以上是顶部导航栏，需要根据页面配置 -->
				</nav>
			</div>
		<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>商品选购</h5>
							<div class="ibox-tools">
								<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
								</a> <a class="dropdown-toggle" data-toggle="dropdown"
									href="table_data_tables.html#"> <i class="fa fa-wrench"></i>
								</a>
								<ul class="dropdown-menu dropdown-user">
									<li><a href="table_data_tables.html#">选项1</a></li>
									<li><a href="table_data_tables.html#">选项2</a></li>
								</ul>
								<a class="close-link"> <i class="fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div class="">
								<a onclick="fnClickAddRow();" href="javascript:void(0);"
									class="btn btn-success ">提交</a>
							</div>
							<table class="table table-striped table-bordered table-hover "
								id="editable">
								<thead>
									<tr>
										<th>产品id</th>
										<th>产品名称</th>
										<th>供应商</th>
										<th>计量单位</th>
										<th>一级分类</th>
										<th>二级分类</th>
										<th>价格</th>
										<th>折扣</th>
										<th>操作</th>

									</tr>
								</thead>
								<tfoot>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
							<p id="append_ins"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 配置点2 -->



    <script>
        $(document).ready(function () {
            $('.contact-box').each(function () {
                animationHover(this, 'pulse');
            });
        });
    </script>




</body>



<!-- 配置点2结束 -->
				
					
                    

	<!-- Mainly scripts -->
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/bootstrap.min.js?v=3.4.0"></script>
	<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>

	<!-- Data Tables -->
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

	<!-- Custom and plugin javascript -->
	<script src="js/plugins/pace/pace.min.js"></script>
	    <!-- Data picker -->
    <script src="js/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>

    <script src="js/plugins/validate/messages_zh.min.js"></script>
	
	
	<script>
			var url="api/products";
		$(document).ready(function() {
			$.ajax({
				type: "GET",
				url: url,
				data: {
				},
				dataType:"json",
				success: function(result){
					var pro=result.data;
					var lg=pro.length;
					
					for(var i=0;i<lg;i++)
					{
						inittable(pro,i);
						initui(pro,i);
					}
				}
				});
			
		//初始化tabel
			$('.dataTables-example').dataTable();

			/* Init DataTables */
			var oTable = $('#editable').dataTable();

			/* Apply the jEditable handlers to the table */
			oTable.$('td').editable('../example_ajax.php', {
				"callback" : function(sValue, y) {
					var aPos = oTable.fnGetPosition(this);
					oTable.fnUpdate(sValue, aPos[0], aPos[1]);
				},
				"submitdata" : function(value, settings) {
					return {
						"row_id" : this.parentNode.getAttribute('id'),
						"column" : oTable.fnGetPosition(this)[2]
					};
				},

				"width" : "90%",
				"height" : "100%"
			});

		});
		
		
		
	function initui(pro,i)
	{
		var str=
		
		"<div class='col-lg-4'>"
		+"<div class='contact-box'>"
		+"<div class='col-sm-4'>"
		+"<div class='text-center'>"
		+"<img alt=\"image\" class=\"img-circle m-t-xs img-responsive\" src=\"images/"+pro[i].id+".jpg\">"
		+"<div class='m-t-xs font-bold'>"
		+pro[i].id
		+"</div></div></div>"
		+"<div class='col-sm-8'><h3><strong>"
		+pro[i].name
		+"</strong></h3>"
		+"<p><i class='fa fa-map-marker'></i>"
		+pro[i].period
		+" <address></p>"
		+"<p>一级分类："
		+pro[i].majorType.name
		+"</p>"
		+"<p>价格："
		+pro[i].price+"元/"+pro[i].unit
		+"</p>"
		+"<p>二级分类："
		+pro[i].minorType.name
		+"</p></address>"
		+"<button onclick='add(id)' class='btn btn-w-m btn-info' id='"+pro[i].id+"'>添加到购物车</button>"
		+"</div> <div class='clearfix'></div></div></div>";
		$("#append_ins").append(str);
	}
		
		
		
		
	function inittable(pro,i)
	{
		$('#editable').dataTable()
		.fnAddData(
			[
			pro[i].id,
			pro[i].name,
			pro[i].supplier.name,
			pro[i].unit,
			pro[i].majorType.name,
			pro[i].minorType.name,
			pro[i].price,
			pro[i].discount/10+"折",
			"<a onclick='add(id)' id='"
			+pro[i].id
			+"'>添加到购物车</a>"
			]);
	}
	var list = new Array();;
	function add( id)
	{
		
		if($.inArray(id, list)  )
		{
			list.push(id);
		}
		alert("添加产品成功");
		//location.href=("hwz_shop_card.jsp");
	}
		function fnClickAddRow() {
			if(list.length>0)
			{
				location.href=("ClientServer/hwz_shop_card.jsp?list="+list+"&id="+<%=id%>);
			}
			else
			{
				alert("您的购物车现在为空，请添加物品！");
			}
		}
		$(document).ready(function () {
	        $('.contact-box').each(function () {
	            animationHover(this, 'pulse');
	        });
		});
	</script>

</body>

</html>

