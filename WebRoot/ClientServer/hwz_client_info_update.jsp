<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<base href="<%=basePath%>">
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="renderer" content="webkit" />
	<link href="css/bootstrap.min.css" rel="stylesheet" />
	<link href="css/font-awesome.min.css" rel="stylesheet" />
	<link href="css/animate.css" rel="stylesheet" />
	<link href="css/style.css" rel="stylesheet" />
	<link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">
	
</head>
<body class="top-navigation">
	<div id="wrapper">
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
<nav class="navbar navbar-static-top" data-role="navigation">
<!-- 以下是顶部导航栏，需要根据页面配置 -->
					<div class="navbar-header">
						<div class="minimalize-styl-2">
						
<!-- 请确保下面的<a>标签中的href属性地址和相对路径一致 -->
							<a class="btn btn-success" title="返回主页" href="main.html"><i class="fa fa-lg fa-home"></i></a>
<!-- 请确保上面的<a>标签中的href属性地址和相对路径一致 -->

							<a class="back-link btn btn-success" title="后退"><i class="fa fa-lg fa-arrow-left"></i></a>
							<a class="reload-link btn btn-success" title="重新加载"><i class="fa fa-lg fa-repeat"></i></a>

<!-- 以下是指示页面上下文的部分，需要根据实际路径修改 -->
							<ol id="guide" class="breadcrumb m-l">
<!-- <li><a>上级页面的上级页面</a><li> -->
<!-- <li><a>上级页面</a><li> -->
<!-- <li><strong>当前页面</strong><li> -->
								<li><strong>主页</strong></li>
							</ol>
<!-- 以上是指示页面上下文的部分，需要根据实际路径修改 -->

						</div>
					</div>
					<div class="nav navbar-top-links navbar-right minimalize-styl-2">
						<span class="m-r-sm text-muted welcome-message btn btn-link">欢迎使用Feces物流管理系统</span>
					</div>
<!-- 以上是顶部导航栏，需要根据页面配置 -->
				</nav>
			</div>
			
			
			
			
			<!-- 配置点2 -->
			<!-- 内容 -->
			<div class="wrapper wrapper-content animated fadeInRight">
			
			
			
			
				<!-- 配置点2.1 -->
				<!-- 段落1 -->
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>查询客户信息</h5>
							</div>
							<div class="ibox-content">
				<!-- 配置点2.2 -->
				<!-- 段落2 -->
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>用户信息</h5>
							</div>
							<div class="ibox-content">
							<form action="api/update_cli">
							<table class="table table-striped table-bordered table-hover "
								id="editable">
							<thead><td></td><td></td></thead>
							<tbody>
							</tbody>
							</table>
							<input type="button" onclick="submit_update()" class="btn btn-success" value="确认修改">
							</form>
							</div>
						</div>
					</div>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="js/feces.js"></script>
	<script src="js/plugins/pace/pace.min.js"></script>
	<script src="js/plugins/toastr/toastr.min.js"></script>
	<!-- Data Tables -->
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	
	
	
	<!-- 配置点3 -->
	<!-- 载入后的js脚本 -->
	<script>
	$(function(){
		// you JavaScript code here!
	});
		var url = "api/cl";
		<% String id=request.getParameter("id");%>
		$(document).ready(function()
		{
			$.ajax({
			type: "GET",
			url: url,
			data: {
				id: <%=id%>
			},
			success: function(result){
				var cli=result.data[0];
				$('#editable').dataTable()
					.fnAddData(["客户编号：",cli.id]);
				$('#editable').dataTable()
					.fnAddData(["客户姓名：","<input type=\"text\" id=\"name\" value=\""+cli.name+"\">"]);
				$('#editable').dataTable()
					.fnAddData(["所在单位：","<input type=\"text\" id=\"bu\" value=\""+cli.bu+"\">"]);
				$('#editable').dataTable()
					.fnAddData(["电话号码：","<input type=\"text\" id=\"mphone\" value=\""+cli.mphone+"\">"]);
				$('#editable').dataTable()
					.fnAddData(["地址：","<input type=\"text\" id=\"address\" value=\""+cli.address+"\">"]);
				$('#editable').dataTable()
					.fnAddData(["身份证号：","<input type=\"text\" id=\"id_card\" value=\""+cli.id_card+"\">"]);
				$('#editable').dataTable()
					.fnAddData(["邮政编码：","<input type=\"text\" id=\"mail\" value=\""+cli.mail+"\">"]);
				$('#editable').dataTable()
					.fnAddData(["电子邮箱：","<input type=\"text\" id=\"email\" value=\""+cli.email+"\">"]);
			}
			
			});
		});
		$(document).ready(function() {
			$('.dataTables-example').dataTable();

			/* Init DataTables */
			var oTable = $('#editable').dataTable();

			/* Apply the jEditable handlers to the table */
			oTable.$('td').editable('../example_ajax.php', {
				"callback" : function(sValue, y) {
					var aPos = oTable.fnGetPosition(this);
					oTable.fnUpdate(sValue, aPos[0], aPos[1]);
				},
				"submitdata" : function(value, settings) {
					return {
						"row_id" : this.parentNode.getAttribute('id'),
						"column" : oTable.fnGetPosition(this)[2]
					};
				},

				"width" : "90%",
				"height" : "100%"
			});

		});

		
		function submit_update()
		{
			$.ajax({
			type: "PUT",
			url: url,
			data: {
				id: <%=id%>,
				name:$("#name").val(),
				bu:$("#bu").val(),
				mphone:$("#mphone").val(),
				phone:$("#phone").val(),
				address:$("#address").val(),
				id_card:$("#id_card").val(),
				mail:$("#mail").val(),
				email:$("#email").val()
			},
			success: function(result){
			alert("success!");
			}
			
			});
		}
		function fnClickAddRow() {
			
		}
	</script>
	
	
	
	</body>
</html>
