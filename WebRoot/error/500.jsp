<%@ page language="java" pageEncoding="UTF-8" %>
<%
String base_path = String.format(
	"%s://%s:%s%s/",
	request.getScheme(),
	request.getServerName(),
	request.getServerPort(),
	request.getContextPath()
);
%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<base href="<%=base_path%>">
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="renderer" content="webkit" />
	<link href="css/bootstrap.min.css" rel="stylesheet" />
	<link href="css/animate.css" rel="stylesheet" />
	<link href="css/style.css" rel="stylesheet" />
</head>
<body class="gray-bg">
	<div class="middle-box text-center animated fadeInDown">
		<h1>500</h1>
		<h3 class="font-bold">服务器内部错误</h3>
		<div class="error-desc">
			服务器好像出错了... <br />
			您可以返回主页看看 <br />
			<a href="main.html" class="btn btn-primary m-t">主页</a>
		</div>
	</div>
	<script src="js/jquery-2.1.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>