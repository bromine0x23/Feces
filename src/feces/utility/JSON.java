package feces.utility;

import java.lang.reflect.Type;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 封装GSON接口的工具类，用于生成和解析JSON串
 *
 * @author Bromine0x23
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JSON {

	private static final Gson GSON = new GsonBuilder().create();
	
	/**
	 * 解析JSON串为目标类的对象
	 * 
	 * @param json JSON串
	 * @param type 目标类型
	 * @return 解析后的目标类对象
	 */
	public static <T> T from(String json, Type type) {
		return GSON.fromJson(json, type);
	}

	/**
	 * 解析JSON串为目标类的对象
	 * 
	 * @param json JSON串
	 * @param klass 目标类
	 * @return 解析后的目标类对象
	 */
	public static <T> T from(String json, Class<T> klass) {
		return GSON.fromJson(json, klass);
	}

	/**
	 * 格式化对象生成JSON串
	 * 
	 * @param object 被格式化对象
	 * @return 生成的JSON串
	 */
	public static <T> String to(T object) {
		return GSON.toJson(object);
	}
	
	/**
	 * 格式化对象生成JSON串写入流中
	 * 
	 * @param object 被格式化对象
	 * @param writer 写者
	 */
	public static <T> void to(T object, Appendable writer) {
		GSON.toJson(object, writer);
	}
}