package feces.utility;

import java.io.PrintStream;
import java.io.PrintWriter;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * IO实用工具
 * 无聊的接口包装
 *
 * @author Bromine0x23
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class IO {

	/**
	 * 刷新流
	 *
	 * @param stream
	 */
	public static void fflush(PrintStream stream) {
		stream.flush();
	}

	/**
	 * 刷新标准输出流
	 *
	 * @see feces.utility.IO#fflush(PrintStream)
	 */
	public static void flush() {
		fflush(System.out);
	}

	/**
	 * 打印对象到流
	 *
	 * @param stream PrintStream对象
	 * @param objects 被打印对象
	 * @see feces.utility.IO#fprint(PrintStream, Object)
	 */
	public static void fprint(PrintStream stream, Object... objects) {
		for (Object object : objects) {
			fprint(stream, object);
		}
	}

	/**
	 * 打印对象
	 *
	 * @param stream PrintStream对象
	 * @param object 被打印对象
	 */
	public static <T> void fprint(PrintStream stream, T object) {
		stream.print(object);
	}

	/**
	 * 打印对象
	 *
	 * @param writer PrintWriter对象
	 * @param objects 被打印对象
	 * @see feces.utility.IO#fprint(PrintWriter, Object)
	 */
	public static void fprint(PrintWriter writer, Object... objects) {
		for (Object object : objects) {
			fprint(writer, object);
		}
	}

	/**
	 * 打印对象
	 *
	 * @param writer PrintWriter对象
	 * @param object 被打印对象
	 */
	public static <T> void fprint(PrintWriter writer, T object) {
		writer.print(object);
	}

	/**
	 * 格式化打印对象
	 *
	 * @param stream PrintStream对象
	 * @param format 格式
	 * @param objects 被打印对象
	 */
	public static void fprintf(PrintStream stream, String format, Object... objects) {
		stream.printf(format, objects);
	}

	/**
	 * 格式化打印对象
	 *
	 * @param writer PrintWriter对象
	 * @param format 格式
	 * @param objects 被打印对象
	 */
	public static void fprintf(PrintWriter writer, String format, Object... objects) {
		writer.printf(format, objects);
	}

	/**
	 * 打印并换行
	 * 
	 * @param stream PrintStream对象
	 */
	public static void fprintln(PrintStream stream) {
		stream.println();
	}

	/**
	 * 打印并换行
	 * 
	 * @param stream PrintStream对象
	 * @param objects 被打印对象
	 * @see IO#fprintln(PrintStream, Object)
	 */
	public static void fprintln(PrintStream stream, Object... objects) {
		for (Object object : objects) {
			fprintln(stream, object);
		}
	}

	/**
	 * 打印并换行
	 *
	 * @param stream PrintStream对象
	 * @param object 被打印对象
	 */
	public static <T> void fprintln(PrintStream stream, T object) {
		stream.println(object);
	}

	/**
	 * 打印并换行
	 *
	 * @param writer PrintWriter对象
	 */
	public static void fprintln(PrintWriter writer) {
		writer.println();
	}

	/**
	 * 打印并换行
	 *
	 * @param writer PrintWriter对象
	 * @param objects 被打印对象
	 * @see IO#fprintln(PrintWriter, Object)
	 */
	public static void fprintln(PrintWriter writer, Object... objects) {
		for (Object object : objects) {
			fprintln(writer, object);
		}
	}

	/**
	 * 打印并换行
	 *
	 * @param writer PrintWriter对象
	 * @param object 被打印对象
	 */
	public static <T> void fprintln(PrintWriter writer, T object) {
		writer.println(object);
	}

	/**
	 * 打印到标准输出流
	 *
	 * @param objects 被打印对象
	 */
	public static void print(Object... objects) {
		fprint(System.out, objects);
	}

	/**
	 * 打印对象到标准输出流
	 *
	 * @param object 被打印对象
	 */
	public static <T> void print(T object) {
		fprint(System.out, object);
	}

	/**
	 * 格式化打印对象到标准输出流
	 *
	 * @param format 格式
	 * @param objects 被打印对象
	 */
	public static void printf(String format, Object... objects) {
		fprintf(System.out, format, objects);
	}

	/**
	 * 向标准输出流输出换行
	 */
	public static void println() {
		println(System.out);
	}

	/**
	 * 打印对象到标准输出流并换行
	 *
	 * @param objects 被打印对象
	 */
	public static void println(Object... objects) {
		fprintln(System.out, objects);
	}

	/**
	 * 打印对象到标准输出流并换行
	 *
	 * @param object 被打印对象
	 */
	public static <T> void println(T object) {
		fprintln(System.out, object);
	}

	/**
	 * 格式化对象为字符串
	 *
	 * @param object 被格式化对象
	 */
	public static String sprintf(String format,Object... objects) {
		return String.format(format, objects);
	}
}
