package feces.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import feces.request.HTTPRequest;

/**
 * 包装请求对象以正确解析PUT和DELETE请求参数
 *
 * @author Bromine0x23
 * @see feces.request.HTTPRequest
 */
public class FacadeFilter implements Filter {

	@Override
	public void destroy() {
		// do nothing
	}

	/**
	 * 包装请求对象
	 */
	@Override
	public void doFilter(
		ServletRequest request,
		ServletResponse response,
		FilterChain chain
	) throws IOException, ServletException {
		chain.doFilter(HTTPRequest.facade(request), response);
	}

	@Override
	public void init(
		FilterConfig config
	) throws ServletException {
		// do nothing
	}

}
