package feces.validator;

/**
 * 校验字符串是否在给定长度内
 * 
 * @author Bromine0x23
 */
public final class StringValidator {

	// 禁止实例化
	private StringValidator() {
		// do nothing
	}

	/**
	 * @param string 待校验的字符串
	 * @param max 长度上限
	 * @return 是否合法
	 */
	public static boolean validate(String string, int max) {
		return validate(string, max, false);
	}

	/**
	 * @param string 待校验的字符串
	 * @param max 长度上限
	 * @param nullable 是否可为空
	 * @return 是否合法
	 */
	public static boolean validate(String string, int max, boolean nullable) {
		return validate(string, 1, max, nullable);
	}
	
	/**
	 * @param string 待校验的字符串
	 * @param min 长度下限
	 * @param max 长度上限
	 * @return 是否合法
	 */
	public static boolean validate(String string, int min, int max) {
		return validate(string, min, max, false);
	}
	
	/**
	 * @param string 待校验的字符串
	 * @param min 长度下限
	 * @param max 长度上限
	 * @param nullable 是否可为空
	 * @return 是否合法
	 */
	public static boolean validate(String string, int min, int max, boolean nullable) {
		return (string != null && min <= string.length() && string.length() <= max) || nullable;
	}
}
