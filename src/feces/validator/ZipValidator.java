package feces.validator;

import java.util.regex.Pattern;

/**
 * 校验所给字符串是否是符合格式的邮编
 *
 * @author Bromine0x23
 */
public final class ZipValidator {

	private static final Pattern PATTERN = Pattern.compile("^\\d{6}$");

	// 禁止实例化
	private ZipValidator() {
		// do nothing
	}
	
	/**
	 * @param string 待校验的值
	 * @return 是否是符合格式的邮编
	 */
	public static boolean validate(String string) {
		return PATTERN.matcher(string).matches();
	}
}
