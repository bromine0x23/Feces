package feces.validator;

import java.util.regex.Pattern;

/**
 * 校验所给字符串是否是符合格式的Email地址
 *
 * @author Bromine0x23
 */
public final class EmailValidator {
	
	private static final Pattern PATTERN = Pattern.compile(
		"^(?:\\w+)(?:\\.\\w+)*@(?:\\w+)(?:\\.\\w+)*$"
	);

	// 禁止实例化
	private EmailValidator() {
		// do nothing
	}

	/**
	 * @param string 待校验的值
	 * @return 是否是符合格式的Email地址
	 */
	public static boolean validate(String string) {
		return PATTERN.matcher(string).matches();
	}
}
