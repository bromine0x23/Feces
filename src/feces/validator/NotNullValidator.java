package feces.validator;

/**
 * 校验对象是否非空
 * 
 * @author Bromine0x23
 */
public final class NotNullValidator {

	// 禁止实例化
	private NotNullValidator() {
		// do nothing
	}

	/**
	 * @param object 待校验的对象
	 * @return 是否非空
	 */
	public static boolean validate(Object object) {
		return object != null;
	}
}
