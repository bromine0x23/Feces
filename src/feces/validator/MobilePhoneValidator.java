package feces.validator;

import java.util.regex.Pattern;

/**
 * 校验所给字符串是否是符合格式的移动电话号码
 * 
 * @author Bromine0x23
 */
public final class MobilePhoneValidator {

	// 禁止实例化
	private MobilePhoneValidator() {
		// do nothing
	}

	private static final Pattern PATTERN = Pattern.compile("^\\d{11}$");
	
	/**
	 * @param string 待校验的值
	 * @return 是否是符合格式的移动电话号码
	 */
	public static boolean validate(String string) {
		return PATTERN.matcher(string).matches();
	}
}
