package feces.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.NoArgsConstructor;

/**
 * 样例伺服器
 * @author Bromine0x23
 */
@WebServlet(name="sample", urlPatterns={"/api/sample"})
@NoArgsConstructor
public class SampleServlet extends RESTfulServlet {

	private static final long serialVersionUID = -831991548854591549L;
	
	/**
	 * 响应POST请求
	 */
	@Override
	protected Object[] responseCreate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return new Object[]{"POST OK", request.getParameterMap()};
	}

	/**
	 * 响应DELETE请求
	 */
	@Override
	protected Object[] responseDelete(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return new Object[]{"DELETE OK", request.getParameterMap()};
	}
	
	/**
	 * 响应GET请求
	 */
	@Override
	protected Object[] responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return new Object[]{"GET OK", request.getParameterMap()};
	}
	
	/**
	 * 响应PUT请求
	 */
	@Override
	protected Object[] responseUpdate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return new Object[]{"PUT OK", request.getParameterMap()};
	}
}
