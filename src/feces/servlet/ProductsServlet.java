package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.model.Product;
import lombok.NoArgsConstructor;

/**
 * 获取所有商品信息
 * 
 * @author Bromine0x23
 */
@WebServlet(name="products", urlPatterns={"/api/products"})
@NoArgsConstructor
public class ProductsServlet extends RESTfulServlet {

	private static final long serialVersionUID = -841294855516590409L;

	/**
	 * 响应GET请求
	 * 获取所有商品
	 * 
	 * @return 所有商品
	 */
	@Override
	protected Response<List<Product>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return succeed(findAll(Product.class));
	}
}
