package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.NoArgsConstructor;
import feces.model.Allocation;
import feces.model.ClientOrderList;
import feces.model.StorehouseIn;
import feces.model.StorehouseOut;
import feces.model.StorehouseProduct;
import feces.utility.JSON;
import feces.utility.Utility;

/**
 * 调拨操作管理器 按照调拨单执行商品调拨操作
 * @author 李则宇
 * 
 */
@WebServlet(name="Allocation", urlPatterns={"/api/allocation"})
@NoArgsConstructor
public class AllocationServlet extends RESTfulServlet {

	private static final long serialVersionUID = -7625377721671294575L;
	
	/**
	 * 相应GET请求
	 * 获取全部调拨单信息
	 */
	@Override
	protected Response<List<Allocation>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return succeed(findAll(Allocation.class));
	}
	/**
	 * 响应PUT请求
	 *  根据操作 (中心出/分站入/领货) 
	 *  	修改调拨单对应的定单状态为 ("中心库房出库"/"配送站到货"/"已领货")
	 * 		修改任务单对应状态为(--/"可分配"/"已领货")
	 *            修改对应仓库库存
	 *            生成出/入库记录
	 * @return 操作结果
	 */
	@SuppressWarnings({ "cast", "unchecked" })
	@Override
	protected Response<Boolean> responseUpdate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		final List<String> parameter = (List<String>)JSON.from(request.getParameter("ids"),List.class);
		if (parameter == null) {
			return invalid(false);
		}
		final String operation = request.getParameter("operation");
		boolean result = doTransaction(new Runnable(){
			
			@Override
			public void run() {
				//如果操作为中心仓库出库
				if("cenSHalloout".equals(operation)){
					for(int z=0;z<parameter.size();z++){
						//修改调拨单对应的定单状态为"中心库房出库"
						Integer id = Utility.toInteger(parameter.get(z));
						Allocation allocation = find(Allocation.class, id);
						allocation.getClientOrder().setStatus("中心库房出库");		
						update(allocation);
						//修改出库仓库库存
						Integer outSHId = allocation.getOutStorehouse().getId();
						List<ClientOrderList> clientOrderLists = allocation.getClientOrder().getProducts();
						for(int pro=0;pro<clientOrderLists.size();pro++){
							Integer proId = clientOrderLists.get(pro).getProduct().getId();
							StorehouseProduct storehouseProduct = find(StorehouseProduct.class, new StorehouseProduct.PrimaryKey(outSHId,proId));
							Integer current = storehouseProduct.getQuantity()-clientOrderLists.get(pro).getProduct_number();
							storehouseProduct.setQuantity(current);
							update(storehouseProduct);
							//生成出库记录
							StorehouseOut storehouseOut = new StorehouseOut();
							storehouseOut.setProduct(clientOrderLists.get(pro).getProduct());
							storehouseOut.setStorehouse(allocation.getOutStorehouse());
							storehouseOut.setAmount(clientOrderLists.get(pro).getProduct_number());
							storehouseOut.setTask(allocation.getTaskVoucher());
							add(storehouseOut);
						}
					}
				}
				//如果操作为分站仓库入库	
				else if("subSHalloin".equals(operation)){
					for(int z=0;z<parameter.size();z++){
						System.out.println(z);
						//修改调拨单对应的定单状态为"配送站到货"
						Integer id = Utility.toInteger(parameter.get(z));
						Allocation allocation = find(Allocation.class, id);
						allocation.getClientOrder().setStatus("配送站到货");		
						//修改调拨单对应的任务状态为"可分配"
						allocation.getTaskVoucher().setTask_state("可分配");
						update(allocation);
						//修改入库仓库库存
						Integer inSHId = allocation.getInStorehouse().getId();
						List<ClientOrderList> clientOrderLists = allocation.getClientOrder().getProducts();
						for(int pro=0;pro<clientOrderLists.size();pro++){
							Integer proId = clientOrderLists.get(pro).getProduct().getId();
							StorehouseProduct storehouseProduct = find(StorehouseProduct.class, new StorehouseProduct.PrimaryKey(inSHId,proId));
							Integer current = storehouseProduct.getQuantity()+clientOrderLists.get(pro).getProduct_number();
							storehouseProduct.setQuantity(current);
							update(storehouseProduct);
							//生成入库记录
							StorehouseIn storehouseIn = new StorehouseIn();
							storehouseIn.setProduct(clientOrderLists.get(pro).getProduct());
							storehouseIn.setStorehouse(allocation.getInStorehouse());
							storehouseIn.setAmount(clientOrderLists.get(pro).getProduct_number());
							storehouseIn.setTask(allocation.getTaskVoucher());
							add(storehouseIn);
						}
					}
				}
				else if("picking".equals(operation)){
					for(int z=0;z<parameter.size();z++){
						System.out.println(z);
						//修改调拨单对应的任务状态为"已领货"
						Integer id = Utility.toInteger(parameter.get(z));
						Allocation allocation = find(Allocation.class, id);		
						allocation.getTaskVoucher().setTask_state("已领货");
						update(allocation);
						//修改分站仓库库存
						Integer inSHId = allocation.getInStorehouse().getId();
						List<ClientOrderList> clientOrderLists = allocation.getClientOrder().getProducts();
						for(int pro=0;pro<clientOrderLists.size();pro++){
							Integer proId = clientOrderLists.get(pro).getProduct().getId();
							StorehouseProduct storehouseProduct = find(StorehouseProduct.class, new StorehouseProduct.PrimaryKey(inSHId,proId));
							Integer current = storehouseProduct.getQuantity()-clientOrderLists.get(pro).getProduct_number();
							storehouseProduct.setQuantity(current);
							update(storehouseProduct);
							//生成出库记录
							StorehouseOut storehouseOut = new StorehouseOut();
							storehouseOut.setProduct(clientOrderLists.get(pro).getProduct());
							storehouseOut.setStorehouse(allocation.getInStorehouse());
							storehouseOut.setAmount(clientOrderLists.get(pro).getProduct_number());
							storehouseOut.setTask(allocation.getTaskVoucher());
							add(storehouseOut);
						}
					}
				}
			}
		});
		return result ? succeed(true) : exception(false);
	}
}
