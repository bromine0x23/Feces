package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.model.ClientOrderList;
import feces.model.Storehouse;
import feces.model.StorehouseIn;
import feces.model.StorehouseOut;
import feces.model.StorehouseProduct;
import feces.model.TaskVoucher;
import feces.utility.JSON;
import feces.utility.Utility;

/**
 * 库房退货管理器 
 * 		执行退货时的库房操作
 * @author 李则宇
 *
 */
@WebServlet(name="lzyReturn", urlPatterns={"/api/lzyReturn"})
public class lzyReturnServlet extends RESTfulServlet{

	private static final long serialVersionUID = 5442501998590332011L;
	private static final Integer centralStorehouseID = 81;
	
	/**
	 * 相应GET请求
	 * 获取全部退货任务单信息
	 */
	@Override
	protected Response<List<TaskVoucher>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return succeed(findAll(TaskVoucher.class));
	}
	
	/**
	 * 响应PUT请求
	 *  根据操作 (分站退货出库/中心站入库/中心站退货出库) 
	 * 		修改任务单对应状态为(分站退货出库/中心退货入库/中心退货出库)
	 *  修改对应仓库库存
	 *  生成出/入库记录
	 * @return 操作结果
	 */
	@SuppressWarnings({ "cast", "unchecked" })
	@Override
	protected Response<Boolean> responseUpdate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		final List<String> parameter = (List<String>)JSON.from(request.getParameter("ids"),List.class);
		if (parameter == null) {
			return invalid(false);
		}
		final String operation = request.getParameter("operation");
		boolean result = doTransaction(new Runnable(){
			
			@Override
			public void run() {
				//如果操作为  分站退货出库
				if("subSHreout".equals(operation)){
					for(int z=0;z<parameter.size();z++){
						//修改任务单状态为"分站退货出库"
						Integer id = Utility.toInteger(parameter.get(z));
						TaskVoucher taskVoucher = find(TaskVoucher.class, id);
						taskVoucher.setTask_type("退货中");		
						taskVoucher.setTask_state("分站退货出库");		
						update(taskVoucher);
						//修改出库仓库库存
						Integer outSHId = taskVoucher.getSubstation().getStorehouse_id();
						List<ClientOrderList> clientOrderLists = taskVoucher.getOrder().getProducts();
						for(int pro=0;pro<clientOrderLists.size();pro++){
							Integer proId = clientOrderLists.get(pro).getProduct().getId();
							StorehouseProduct storehouseProduct = find(StorehouseProduct.class, new StorehouseProduct.PrimaryKey(outSHId,proId));
							Integer current = storehouseProduct.getQuantity()-clientOrderLists.get(pro).getProduct_number();
							storehouseProduct.setQuantity(current);
							update(storehouseProduct);
							//生成出库记录
							StorehouseOut storehouseOut = new StorehouseOut();
							storehouseOut.setProduct(clientOrderLists.get(pro).getProduct());
							storehouseOut.setStorehouse(find(Storehouse.class, 1));
							storehouseOut.setAmount(clientOrderLists.get(pro).getProduct_number());
							storehouseOut.setTask(taskVoucher);
							add(storehouseOut);
						}
					}
				}
				//如果操作为中心站入库	
				else if("cenSHrein".equals(operation)){
					for(int z=0;z<parameter.size();z++){
						//修改任务单状态为"中心站入库"
						Integer id = Utility.toInteger(parameter.get(z));
						TaskVoucher taskVoucher = find(TaskVoucher.class, id);
						taskVoucher.setTask_state("中心站入库");		
						update(taskVoucher);
						//修改入库仓库库存
						Integer inSHId = centralStorehouseID;
						List<ClientOrderList> clientOrderLists = taskVoucher.getOrder().getProducts();
						for(int pro=0;pro<clientOrderLists.size();pro++){
							Integer proId = clientOrderLists.get(pro).getProduct().getId();
							StorehouseProduct storehouseProduct = find(StorehouseProduct.class, new StorehouseProduct.PrimaryKey(inSHId,proId));
							Integer current = storehouseProduct.getQuantity()+clientOrderLists.get(pro).getProduct_number();
							storehouseProduct.setQuantity(current);
							update(storehouseProduct);
							//生成入库记录
							StorehouseIn storehouseIn = new StorehouseIn();
							storehouseIn.setProduct(clientOrderLists.get(pro).getProduct());
							storehouseIn.setStorehouse(find(Storehouse.class, 3));
							storehouseIn.setAmount(clientOrderLists.get(pro).getProduct_number());
							storehouseIn.setTask(taskVoucher);
							add(storehouseIn);
						}
					}
				}
				//如果操作为中心退货出库	
				else if("cenSHreout".equals(operation)){
					for(int z=0;z<parameter.size();z++){
						//修改任务单状态为"中心退货出库"
						Integer id = Utility.toInteger(parameter.get(z));
						TaskVoucher taskVoucher = find(TaskVoucher.class, id);
						taskVoucher.setTask_state("中心退货出库");		
						update(taskVoucher);
						//修改出库仓库库存
						Integer outSHId = centralStorehouseID;
						List<ClientOrderList> clientOrderLists = taskVoucher.getOrder().getProducts();
						for(int pro=0;pro<clientOrderLists.size();pro++){
							Integer proId = clientOrderLists.get(pro).getProduct().getId();
							StorehouseProduct storehouseProduct = find(StorehouseProduct.class, new StorehouseProduct.PrimaryKey(outSHId,proId));
							Integer current = storehouseProduct.getQuantity()-clientOrderLists.get(pro).getProduct_number();
							storehouseProduct.setQuantity(current);
							update(storehouseProduct);
							//生成出库记录
							StorehouseOut storehouseOut = new StorehouseOut();
							storehouseOut.setProduct(clientOrderLists.get(pro).getProduct());
							storehouseOut.setStorehouse(find(Storehouse.class, 3));
							storehouseOut.setAmount(clientOrderLists.get(pro).getProduct_number());
							storehouseOut.setTask(taskVoucher);
							add(storehouseOut);
						}
					}
				}
			}
		});
		return result ? succeed(true) : exception(false);
	}
}
