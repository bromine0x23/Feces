package feces.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.model.Supplier;
import feces.utility.Utility;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 供应商管理
 *
 * @author Bromine0x23
 */
@WebServlet(name="supplier", urlPatterns={"/api/supplier"})
@NoArgsConstructor
public class SupplierServlet extends RESTfulServlet {

	private static final long serialVersionUID = -5972206744011319108L;

	/**
	 * 字典
	 * 
	 * @author Bromine0x23
	 */
	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class Dictionary {
		public static final String ID = "id";

		public static final String NAME = "name";
		
		public static final String ADDRESS = "address";

		public static final String CONTACT = "contact";

		public static final String CONTACT_PHONE = "contact_phone";

		public static final String BANK = "bank";

		public static final String BANK_ACCOUNT = "bank_account";

		public static final String FAX = "fax";

		public static final String ZIP = "zip";

		public static final String CORPORATION = "corporation";

		public static final String REMARK = "remark";
	}

	/**
	 * 响应POST请求
	 * 创建供应商
	 * 
	 * @return 供应商代码
	 */
	@Override
	protected Response<Integer> responseCreate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		final Supplier supplier = new Supplier();
		supplier.setName(request.getParameter(Dictionary.NAME));
		supplier.setAddress(request.getParameter(Dictionary.ADDRESS));
		supplier.setContact(request.getParameter(Dictionary.CONTACT));
		supplier.setContactPhone(request.getParameter(Dictionary.CONTACT_PHONE));
		supplier.setBank(request.getParameter(Dictionary.BANK));
		supplier.setBankAccount(request.getParameter(Dictionary.BANK_ACCOUNT));
		supplier.setFax(request.getParameter(Dictionary.FAX));
		supplier.setZip(request.getParameter(Dictionary.ZIP));
		supplier.setCorporation(request.getParameter(Dictionary.CORPORATION));
		supplier.setRemark(request.getParameter(Dictionary.REMARK));;
		if (supplier.isValid()) {
			boolean result = doTransaction(new Runnable(){

				@Override
				public void run() {
					add(supplier);
				}

			});
			if (result) {
				return succeed(supplier.getId());
			}
			return exception(null);
		}
		return failed(null);
	}

	/**
	 * 响应DELETE请求
	 * 删除供应商
	 * 
	 * @return 操作结果
	 */
	@Override
	protected Response<Boolean> responseDelete(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return responseDeleteBasic(request, Supplier.class);
	}
	
	/**
	 * 响应GET请求
	 * 获取供应商
	 * 
	 * @return 查询结果，可能为空
	 */
	@Override
	protected Response<Supplier> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return responseReadBasic(request, Supplier.class);
	}
	
	/**
	 * 响应PUT请求
	 * 修改供应商
	 * 
	 * @return 操作结果
	 */
	@Override
	protected Response<Boolean> responseUpdate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		String parameter = request.getParameter(Dictionary.ID);
		if (parameter == null) {
			return invalid(false);
		}
		Integer id = Utility.toInteger(parameter);
		if (id == null) {
			return invalid(false);
		}
		final Supplier supplier = find(Supplier.class, id);
		if (null != (parameter = request.getParameter(Dictionary.NAME))) {
			supplier.setName(parameter);
		}
		if (null != (parameter = request.getParameter(Dictionary.ADDRESS))) {
			supplier.setAddress(parameter);
		}
		if (null != (parameter = request.getParameter(Dictionary.CONTACT))) {
			supplier.setContact(parameter);
		}
		if (null != (parameter = request.getParameter(Dictionary.CONTACT_PHONE))) {
			supplier.setContactPhone(parameter);
		}
		if (null != (parameter = request.getParameter(Dictionary.BANK))) {
			supplier.setBank(parameter);
		}
		if (null != (parameter = request.getParameter(Dictionary.BANK_ACCOUNT))) {
			supplier.setBankAccount(parameter);
		}
		if (null != (parameter = request.getParameter(Dictionary.FAX))) {
			supplier.setFax(parameter);
		}
		if (null != (parameter = request.getParameter(Dictionary.ZIP))) {
			supplier.setZip(parameter);
		}
		if (null != (parameter = request.getParameter(Dictionary.CORPORATION))) {
			supplier.setCorporation(parameter);
		}
		if (null != (parameter = request.getParameter(Dictionary.REMARK))) {
			supplier.setRemark(parameter);
		}
		if (supplier.isValid()) {
			boolean result = doTransaction(new Runnable(){

				@Override
				public void run() {
					update(supplier);
				}

			});
			return result ? succeed(true) : exception(false);
		}
		return failed(false);
	}

}
