package feces.servlet;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.model.Product_;
import feces.model.StoreRecord;
import feces.model.StoreRecord_;
import feces.model.Storehouse_;
import feces.utility.Utility;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 库房记录伺服器
 * @author Bromine0x23
 */
@WebServlet(name="store_records", urlPatterns={"/api/store_records"})
@NoArgsConstructor
public class StoreRecordsServlet extends RESTfulServlet {

	private static final long serialVersionUID = -2846522592090748934L;

	/**
	 * 字典
	 * 
	 * @author Bromine0x23
	 */
	@NoArgsConstructor(access = AccessLevel.PROTECTED)
	protected static class Dictionary extends RESTfulServlet.Dictionary {
		public static final String STOREHOUSE_ID = "storehouse_id";
		
		public static final String PRODUCT_ID = "product_id";
		
		public static final String START = "start";
		
		public static final String END = "end";
	}

	/**
	 * 响应GET请求
	 * 默认返回库房记录
	 * 
	 * @return 满足条件的库房记录
	 */
	@Override
	protected Response<List<StoreRecord>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<StoreRecord> query = builder.createQuery(StoreRecord.class);
		Root<StoreRecord> root = query.from(StoreRecord.class);
		String parameter;
		parameter = request.getParameter(Dictionary.STOREHOUSE_ID);
		if (!Utility.isEmpty(parameter)) {
			Integer id = Utility.toInteger(parameter);
			if (id == null) {
				return invalid(null);
			}
			query.where(
				builder.equal(
					root.get(StoreRecord_.storehouse).get(Storehouse_.id), id
				)
			);
		}
		parameter = request.getParameter(Dictionary.PRODUCT_ID);
		if (!Utility.isEmpty(parameter)) {
			Integer id = Utility.toInteger(parameter);
			if (id == null) {
				return invalid(null);
			}
			query.where(
				builder.equal(
					root.get(StoreRecord_.product).get(Product_.id), id
				)
			);
		}
		parameter = request.getParameter(Dictionary.START);
		if (!Utility.isEmpty(parameter)) {
			Date date = Utility.toDate(parameter);
			if (date == null) {
				return invalid(null);
			}
			query.where(
				builder.greaterThan(
					root.get(StoreRecord_.created), date
				)
			);
		}
		parameter = request.getParameter(Dictionary.END);
		if (!Utility.isEmpty(parameter)) {
			Date date = Utility.toDate(parameter);
			if (date == null) {
				return invalid(null);
			}
			query.where(
				builder.lessThan(
					root.get(StoreRecord_.created), date
				)
			);
		}
		return succeed(executeQuery(query));
	}
}
