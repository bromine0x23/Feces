package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.NoArgsConstructor;
import feces.model.TaskVoucher;

@WebServlet(name="taskorder",urlPatterns="/api/taskorder")
@NoArgsConstructor
public class TaskOrderServlet extends RESTfulServlet {
	
	private static final long serialVersionUID = -2149167094886179134L;

	/**
	 * 任务单管理
	 * 响应get请求
	 */
	@Override
	protected Response<List<TaskVoucher>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return succeed(findAll(TaskVoucher.class));
	}
	

}
