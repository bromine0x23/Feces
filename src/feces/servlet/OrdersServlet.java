package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.model.ClientOrder;
import feces.servlet.RESTfulServlet.Response;

@WebServlet(name="orders",urlPatterns="/api/orders")
public class OrdersServlet extends RESTfulServlet {

	
	
	
	
	protected Response<List<ClientOrder>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		
		return succeed(findAll(ClientOrder.class));
	}
}
