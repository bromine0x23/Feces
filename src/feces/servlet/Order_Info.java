package feces.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;






import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.mail.iap.Response;

import feces.Database;
import feces.model.Client;
import feces.model.ClientOrder;
import feces.model.ClientOrderList;
import feces.model.Product;
import lombok.NoArgsConstructor;
@WebServlet(name="Orderclient", urlPatterns={"/api/ordercli"})
@NoArgsConstructor
public class Order_Info extends HttpServlet {/**
	 * 
	 */
	private static final long serialVersionUID = -4838686939529217934L;
/**
	/**
	 * Destruction of the servlet. <br>
	 */
	private static final String CONTENT_TYPE = "text/html; charset=utf-8";
	private String id=null; 
	private List <ClientOrder> clol=null;
	private PrintWriter out=null;
	private Client cl;
	
	@Override
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	@Override
	public void doGet(
		HttpServletRequest request, 
		HttpServletResponse response)
		throws ServletException, IOException 
	{
		
		//初始化对象
		response.setContentType(CONTENT_TYPE);
		
		out = response.getWriter();
		
		//获取对象的详细信息
		id=request.getParameter("order_client");
		clol=getInfo();
		
		//展示对象信息
		 showinfo();
		 out.flush();
		 out.close();
	}

	private void showinfo() {
		// TODO 展示对象的详细信息
		String name = cl.getName();
		out.println("<html>");
		out.println("<head><title>"
		+name
		+"的详细信息展示</title>"
		+"<meta charset=\"utf-8\">"
		+"<meta name=\"viewport\" content=\"width=device-width, initial-scale=\"1.0\">"
		+"<meta name='renderer' content='webkit'>"
		+"<link href=\"../css/bootstrap.min.css?v=3.4.0\" rel=\"stylesheet\">"
		+"<link href=\"../css/font-awesome.min.css\" rel=\"stylesheet\">"
		+"<link href=\"../css/animate.css\" rel=\"stylesheet\">"
		+"<link href=\"../css/style.css?v=2.2.0\" rel=\"stylesheet\">"
		+"</head>");
		
		
		out.println("<div id=\"wrapper\"> <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">");
		
		//表头
		out.println("<div class=\"row wrapper border-bottom white-bg page-heading\">");
		out.println ("<div class=\"col-lg-10\"><h2>个人资料</h2><ol class=\"breadcrumb\">");
		out.println("<li><a href=\"index.html\">主页</a></li>");
		out.println("<li><strong>个人资料</strong></li>");
		out.println("</ol></div><div class=\"col-lg-2\"></div></div>");
		out.println("<body class=\"top-navigation\">");
		//个人信息模块
		out.println("<div class=\"col-md-4\">");
		out.println("<div class=\"ibox float-e-margins\">");
		
		out.println("<div class=\"ibox-title\">");
		out.println("<h5>个人资料</h5></div><div>");
		out.println("<div class=\"ibox-content no-padding border-left-right\"><img alt=\"image\" class=\"img-responsive\" src=\"../images/Mark.jpg\"></div>");
		out.println("<div class=\"ibox-content profile-content\">");
		out.println("<h4><strong>"+name+"</strong></h4>");
		out.println("<p>地址:<i class=\"fa fa-map-marker\"></i> "+cl.getAddress()+"</p>");
		out.println("<h5>关于我</h5>");
		
		out.println("单位:"+cl.getBu());
		out.println("</br>身份证号："+cl.getId_card());
		out.println("</br>电话："+cl.getMphone());
		out.println("<h5>联系我：</h5>");
		out.println("邮政编码："+cl.getMail());
		out.println("</br>电子邮件："+cl.getEmail());
		out.println("</div></div></div></div>");
		//订单信息模块
		//订单头
		out.println("<div class=\"col-md-8\">");
		out.println("<div class=\"ibox float-e-margins\">");
		out.println("<h5>最新动态</h5>");
		out.println("<div class=\"ibox-tools\">");
		out.println("<a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>");
		out.println("<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"profile.html#\"><i class=\"fa fa-wrench\"></i></a>");
		out.println("<ul class=\"dropdown-menu dropdown-user\"><li><a href=\"profile.html#\">选项1</a></li><li><a href=\"profile.html#\">选项2</a></li></ul>");
		out.println("<a class=\"close-link\"><i class=\"fa fa-times\"></i></a>");
		out.println("</div></div>");
		//订单信息
		out.println("<div class=\"ibox-content\"><div><div class=\"feed-activity-list\">");
		
		out.println("<div id=\"vertical-timeline\" class=\"vertical-container light-timeline\">");
		for(int i=0;i<clol.size();i++)
		{
			ClientOrder clo = clol.get(i);
			List<ClientOrderList> pros = clo.getProducts();
			//开头
			out.println("<div class=\"vertical-timeline-block\">");
			//out.println("<div class=\"feed-element\">");
			//左边订单编号
			out.println("<div class=\"vertical-timeline-icon navy-bg id=\"order"+i+"\"></div>");
			//中间订单信息
			out.println("<div class=\"vertical-timeline-content\">");
			out.println("<h4>订单</h4>");
			out.println("<div><small>产品信息:</small>");
			int lg = pros.size();
			out.println("<table class=\"table table-striped\">");
			out.println("<thead><tr><th>#</th><th>产品名称</th><th>单价</th><th>数量</th><th>总价</th></tr></thead>");
			for(int j=0;j<lg;j++)
			{
				ClientOrderList prol = pros.get(j);
				Product pro = prol.getProduct();
				out.println("<tr>");
				
				out.println("<td>"+(j+1)+"</td>"
							+"<td>"+pro.getName()+"</td>"
							+"<td>"+pro.getPrice()+"</td>"
							+"<td>"+prol.getProduct_number()+"</td>"
							+"<td>"+pro.getPrice()*prol.getProduct_number()+"</td>");
				
				out.println("</tr>");
			}
			out.println("</table>");
			out.println("</div>");
			out.println("<p>备注："+clo.getRemark()+"</p>");
			out.println("<p>是否支付:"+clo.getIs_pay()+"</p>");
			out.println("<a href=\"../ClientServer/hwz_back_order.jsp?id="+clo.getId()+"#\" class=\"btn btn-sm btn-primary\"> 退订</a>");
			out.println("<span class=\"vertical-date\">日期:<br><small>二月二</small></span>");
			
			out.println("</div>");
			//结束
			out.println("</div>");
		}
		out.println("</div>");
		
		
		
		out.println("</div></div></div>");
		//详细信息结束
		out.println("</div></div>");
		
		//结束
		out.println("</div>");
		out.println("</body></html>");
		
		
		
		
		//资源
		out.println("<!-- Mainly scripts -->");
	    out.println("<script src=\"../js/jquery-2.1.1.min.js\"></script>");
	    out.println("<script src=\"../js/bootstrap.min.js?v=3.4.0\"></script>");
	    out.println("<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>");
	    out.println("<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>");
	    out.println("<!-- Custom and plugin javascript -->");
	    out.println("<script src=\"../js/hplus.js?v=2.2.0\"></script>");
	    out.println("<script src=\"../js/plugins/pace/pace.min.js\"></script>");
	    
	    out.println("<!-- Peity -->");
	    out.println("<script src=\"../js/plugins/peity/jquery.peity.min.js\"></script>");
	    out.println("<script src=\"../js/demo/peity-demo.js\"></script>");

	    
		
		
		out.close();
	}

	private List<ClientOrder> getInfo() {
		// TODO Auto-generated method stub
		List<ClientOrder> clo=null;
		
		if(id.isEmpty()==false)
		{
				cl = Database.find(Client.class, Integer.parseInt(id));
				clo=Database.executeQuery(
				"SELECT DISTINCT e FROM ClientOrder e WHERE e.order_client=?1",
				ClientOrder.class,cl);
		}
		return clo;
	}

}
