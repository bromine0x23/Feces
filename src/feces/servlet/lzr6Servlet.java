package feces.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.persistence.sessions.server.ServerSession;

import feces.Database;
import feces.model.Client;
import feces.model.TaskVoucher;

public class lzr6Servlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public lzr6Servlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("1111111111111");
		response.setHeader("Cache-Control", "no-cache");	
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		request.setCharacterEncoding("utf-8");
		//System.out.println("1111111111111");
		String date=new String((request.getParameter("datename")).getBytes("iso-8859-1"),"utf-8");
		String type=new String((request.getParameter("typename")).getBytes("iso-8859-1"),"utf-8");
		//String deliver=new String((request.getParameter("deliver")).getBytes("iso-8859-1"),"utf-8");
		String deliver1=request.getParameter("deliver");
		int deliver=Integer.parseInt(deliver1.toString().trim());
		System.out.println("date:"+date);
		
		System.out.println("type:"+type);
		System.out.println("deliver:"+deliver);
		Pattern pattern=Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}");
		Matcher m=pattern.matcher(date);
		boolean dateFlag=m.matches();
		Date d=new Date();
		if(!dateFlag&&!date.equals("所有时间")){
			System.out.println("格式错误");
			System.out.println("111111111111111111");
			response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/lzr_search2.jsp");
			//request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/lzrin.jsp"

			
		}
		else if(date.equals("所有时间")){
			System.out.println("所有时间");
		}
		else{
			System.out.println("格式正确");
			 
		    SimpleDateFormat formater = new SimpleDateFormat();  
		    formater.applyPattern("yyyy-MM-dd");  
		    try {  
		            d = formater.parse(date);  
		    } catch (ParseException e) {  
		            e.printStackTrace();  
		    }
		     System.out.println(d);
		}
		List<TaskVoucher> ltv=new ArrayList<TaskVoucher>();
		String state="已分配";
		if(date.equals("所有时间")&&type.equals("所有类型")&&deliver==0){
			
		
			ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.task_state=?1", TaskVoucher.class,state);
			System.out.println("444444444444");
			//System.out.println("ltv 0 is "+ltv.get(0));
			//System.out.println("ltv 0 is "+ltv.get(0).getDeliver());
			//System.out.println(ltv.get(0).getSubstation().getSubstation_id());
			//System.out.println(ltv.get(0).getSubstation().getSubstation_name());
		
			//RequestDispatcher rd=request.getRequestDispatcher("../lzr_search_result.jsp");
			//rd.forward(request,response);

		}
		else if(date.equals("所有时间")&&deliver==0){
		System.out.println(type);
		ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.task_type=?1 AND e.task_state=?2", TaskVoucher.class,type,state);
		}
		else if(date.equals("所有时间")&&type.equals("所有类型")){
			System.out.println(deliver);
			ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.deliver=?1", TaskVoucher.class,deliver);
		}
		else if(deliver==0&&type.equals("所有类型")){
			System.out.println(date);
			ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.date=?1 AND e.task_state=?2", TaskVoucher.class,d,state);
		}
		else if(date.equals("所有时间")){
			System.out.println(type+"-and-"+deliver);
			ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.task_type=?1 AND e.deliver=?2", TaskVoucher.class,type,deliver);
		}
		else if(deliver==0){
			System.out.println(type+"-and-"+date);
			ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.task_type=?1 AND e.date=?2 AND e.task_state=?3", TaskVoucher.class,type,d,state);
		}
		else if(type.equals("所有类型")){
			System.out.println(deliver+"-and-"+date);
			ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.deliver=?1 AND e.date=?2", TaskVoucher.class,deliver,d);
		}
		else{
			System.out.println(deliver+"-and-"+date+"-and-"+type);
			ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.deliver=?1 AND e.date=?2 AND e.task_type=?3", TaskVoucher.class,deliver,d,type);
		}
		int length=ltv.size();
		System.out.println(length);
		HttpSession session = request.getSession();
		session.setAttribute("attr1",ltv);
		System.out.println("test:");
		//System.out.println(ltv.get(0).getOrder().getProducts().get(0).getOrder_id());
		//System.out.println(ltv.get(0).getSubstation().getSubstation_name());
		response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/lzr_print_result.jsp");
		
		
		
		
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
