package feces.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.Database;
import feces.model.ClientBackOrder;
import feces.model.ClientOrder;
import lombok.NoArgsConstructor;
@WebServlet(name="exchangeproduct", urlPatterns={"/api/excpro"})
@NoArgsConstructor
public class Exchange_Pro extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = 471485555611228751L;

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doDelete method of the servlet. <br>
	 *
	 * This method is called when a HTTP delete request is received.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doDelete(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		// Put your code here
	}
	
	
	private static final String CONTENT_TYPE = "text/html; charset=utf-8";

	private ClientOrder cl;

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		//初始化对象
		response.setContentType(CONTENT_TYPE);
		String str = request.getParameter("back_info");
		String idstr=request.getParameter("id");
		String ty=request.getParameter("info");
		String back_order=new String(str.getBytes("iso-8859-1"),"utf-8");
		String id=new String(idstr.getBytes("iso-8859-1"),"utf-8");
		String type=new String(ty.getBytes("iso-8859-1"),"utf-8");
		if((id!=null)&&(id.isEmpty()==false))
		{
			cl = Database.find(ClientOrder.class, Integer.parseInt(id));
		}
		final ClientBackOrder cbo=new ClientBackOrder();
		cl.setType(type);
		System.out.println(cl.getType());
		cbo.setClientorder(cl);
		cbo.setOrderid(Integer.parseInt(id));
		cbo.setReason(back_order);
		if (cbo.isValid()) {
			boolean result =  Database.doTransaction(new Runnable() {
					
				@Override
				public void run() {
					System.out.println("add succeed");
					Database.add(cbo);
					Database.update(cl);
					System.out.println("update succeed");
				}

			});
			//return false;
		}
		response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/ClientServer/hwz_client_info.html");
		//return false;
		
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
