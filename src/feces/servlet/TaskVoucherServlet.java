package feces.servlet;
/*
 * 任务单伺服器
 */

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@WebServlet(name="taskvoucher",urlPatterns="/api/taskvoucher")
public class TaskVoucherServlet extends RESTfulServlet {

	/**
	 * Constructor of the object.
	 */
	public TaskVoucherServlet() {
		super();
	}
	
	@NoArgsConstructor(access=AccessLevel.PROTECTED)
	public static class Direction extends RESTfulServlet.Dictionary{
public static final String NAME = "name";
		
		public static final String ID = "id";
		
		public static final String ORDER_ID = "order_id";
		
		public static final String CLIENT_NAME = "client_name";
		
		public static final String PNAME = "pname";
		
		public static final String PNUM	= "pnum";
		
		public static final String FIN_DATE = "fin_date";
		
		public static final String TYPE = "type";
		
		public static final String STATUS = "status";
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}



	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
