package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.Database;
import feces.model.Allocation;
import feces.model.OutboundOrder;
import feces.model.PurchaseOrder;
import feces.model.Supplier;

/**
 * 出库单管理伺服器
 * @author Bromine0x23
 */
public class OutboundOrderServlet extends RESTfulServlet {

	private static final long serialVersionUID = 3269433931702100232L;

	public OutboundOrderServlet() {
		super();
	}

	/**
	 * 响应GET请求
	 */
	@Override
	protected Response<List<PurchaseOrder>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return succeed(Database.findAll(PurchaseOrder.class));
	}
	
}
