package feces.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.Database;
import feces.model.Client;
import feces.model.ClientOrder;
import feces.model.ClientOrderList;
import feces.servlet.OrderServlet.Directory;
import feces.servlet.RESTfulServlet.Response;
import feces.utility.Utility;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 订单的处理文档
 * 
 */
@WebServlet(name="cl", urlPatterns={"/api/cl"})
@NoArgsConstructor
public class ClientServlet extends RESTfulServlet{

	/**
	 * 
	 * 字典
	 */
	@NoArgsConstructor(access=AccessLevel.PRIVATE)
	public static final class Directory{
		public static final String ID = "id";
		public static final String NAME="name";
		public static final String BU="bu";
		public static final String PHONE="phone";
		public static final String MPHONE="mphone";
		public static final String ID_CARD="id_card";
		public static final String ADDRESS="address";
		public static final String MAIL="mail";
		public static final String EMAIL="email";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4503494315891106500L;
	/**
	 * 响应POST请求
	 * 创建客户
	 * 
	 * @return 客户id
	 */
	@Override
	protected Response<Integer> responseCreate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		String idc = request.getParameter(Directory.ID_CARD);
		if (idc == null) {
			return invalid(null);
		}
		String mp = request.getParameter(Directory.MPHONE);
		if (mp == null) {
			return invalid(null);
		}
		String name = request.getParameter(Directory.NAME);
		if (name == null) {
			return invalid( null);
		}
		final Client cl = new Client();
		String p = request.getParameter(Directory.NAME);
		cl.setName(p);
		p=request.getParameter(Directory.ADDRESS);
		if(p==null)
		{
			p="null";
		}
		cl.setAddress(p);
		p=request.getParameter(Directory.BU);
		if(p==null)
		{
			p="null";
		}
		cl.setBu(p);
		p=request.getParameter(Directory.EMAIL);
		if(p==null)
		{
			p="null";
		}
		cl.setEmail(p);
		
		cl.setId_card(request.getParameter(Directory.ID_CARD));
		p=request.getParameter(Directory.MAIL);
		if(p==null)
		{
			p="null";
		}
		cl.setMail(p);
		cl.setMphone(request.getParameter(Directory.MPHONE));
		p=request.getParameter(Directory.PHONE);
		if(p==null)
		{
			p="null";
		}
		cl.setPhone(p);
		if (cl.isValid()) {
			boolean result = doTransaction(new Runnable() {

				@Override
				public void run() {
					add(cl);
				}

			});
			if (result) {
				return succeed(cl.getId());
			}
			return exception(null);
		}
		return failed(null);
	}

	/**
	 * 响应DELETE请求
	 * 删除用户信息
	 * 
	 * @return 操作结果
	 */
	@Override
	protected Response<Boolean> responseDelete(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		String parameter = request.getParameter(Directory.ID);
		if (parameter == null) {
			return invalid(false);
		}
		Integer id = Utility.toInteger(parameter);
		if (id == null) {
			return invalid(false);
		}
		final Client clo = find(Client.class, id);
		boolean result = doTransaction(new Runnable(){

			@Override
			public void run() {
				remove(clo);
			}
			
		});
		return result ? succeed(true) : exception(false);
	}
	/**
	 * 响应GET请求 获取用户信息
	 * 
	 * @return 查询结果，可能为空
	 */
	@SuppressWarnings("unused")
	@Override
	protected Response<List<Client>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		String p1 = request.getParameter(Directory.ID);
		String p2 = request.getParameter(Directory.NAME);
		String p3 = request.getParameter(Directory.ID_CARD);
		String p4 = request.getParameter(Directory.MPHONE);
		if ((p1 == null)&&(p2 == null)&&(p3 == null)&&(p4 == null)) {
			return invalid(null);
		}
		List<Client> al=new ArrayList<Client>();
		Client clo=null;
		if(p1!=null)
		{
			Integer id = Utility.toInteger(p1);
			if (id == null) 
			{
				return invalid(null);
			}
			clo = find(Client.class, id);
			al.add(clo);
		}
		
		if(p2!=null)
		{
			List<Client> al2 = executeQuery(
				"SELECT DISTINCT e FROM Client e WHERE e.name = ?1", Client.class,p2);
			al.addAll(al2);

		}
		
		if(p3!=null)
		{
			List<Client> al3 = executeQuery(
				"SELECT DISTINCT e FROM Client e WHERE e.id_card = ?1", Client.class,p3);
			if(al.containsAll(al3)==false)
				al.addAll(al3);
			
		}
		if(p4!=null)
		{
			List<Client> al4 = executeQuery(
				"SELECT DISTINCT e FROM Client e WHERE e.mphone = ?1", Client.class,p4);
			if(al.containsAll(al4)==false)
				al.addAll(al4);
			
		}

		if (al != null) {
			return succeed(al);
		}
		return failed(null);
	}
	/**
	 * 响应PUT请求 修改定单
	 * 
	 * @return 操作结果
	 */
	@Override
	protected Response<Boolean> responseUpdate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		String parameter = request.getParameter(Directory.ID);
		if (parameter == null) {
			return invalid(false);
		}
		Integer id = Utility.toInteger(parameter);
		if (id == null) {
			return invalid(false);
		}
		final Client type = find(Client.class, id);
		parameter = request.getParameter(Directory.ADDRESS);
		
		if (parameter != null) {
			type.setAddress(parameter);
		}
		parameter = request.getParameter(Directory.BU);
		if(parameter != null)
		{
			type.setBu(parameter);
		}
		parameter = request.getParameter(Directory.EMAIL);
		if(parameter != null)
		{
			type.setEmail(parameter);
		}
		parameter = request.getParameter(Directory.ID_CARD);
		if(parameter != null)
		{
			type.setId_card(parameter);
		}
		parameter = request.getParameter(Directory.MAIL);
		if(parameter != null)
		{
			type.setMail(parameter);
		}
		parameter = request.getParameter(Directory.MPHONE);
		if(parameter != null)
		{
			type.setMphone(parameter);
		}
		parameter = request.getParameter(Directory.NAME);
		if(parameter != null)
		{
			type.setName(parameter);
		}
		parameter = request.getParameter(Directory.PHONE);
		if(parameter != null)
		{
			type.setPhone(parameter);
		}
		if (type.isValid()) {
			boolean result = doTransaction(new Runnable() {

				@Override
				public void run() {
					update(type);
				}

			});
			return result ? succeed(true) : exception(false);
		}
		return failed(false);
	}
}
