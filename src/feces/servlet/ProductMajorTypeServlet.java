package feces.servlet;

import java.io.IOException;

import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.model.ProductMajorType;
import feces.utility.Utility;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 商品一级分类管理伺服器
 * 
 * @author Bromine0x23
 */
@WebServlet(name="product_major_type", urlPatterns={"/api/product_major_type"})
@NoArgsConstructor
public class ProductMajorTypeServlet extends RESTfulServlet {

	
	/**
	 * 字典
	 * 
	 * @author Bromine0x23
	 */
	@NoArgsConstructor(access = AccessLevel.PROTECTED)
	protected static class Dictionary extends RESTfulServlet.Dictionary {
		
		public static final String NAME = "name";
	}

	private static final long serialVersionUID = -2625191628410018243L;

	/**
	 * 响应POST请求
	 * 创建商品一级分类
	 * 
	 * @return 分类代码
	 */
	@Override
	protected Response<ProductMajorType> responseCreate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		final ProductMajorType type = new ProductMajorType();
		type.setName(request.getParameter(Dictionary.NAME));
		if (type.isValid()) {
			boolean result = doTransaction(new Runnable(){

				@Override
				public void run() {
					add(type);
				}
				
			});
			if (result) {
				return succeed(type);
			}
			return exception(null);
		}
		return failed(null);
	}

	/**
	 * 响应DELETE请求
	 * 删除商品一级分类
	 * 
	 * @return 操作结果
	 */
	@Override
	protected Response<Boolean> responseDelete(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return responseDeleteBasic(request, ProductMajorType.class);
	}
	
	/**
	 * 响应GET请求
	 * 获取商品一级分类
	 * 
	 * @return 查询结果，可能为空
	 */
	@Override
	protected Response<ProductMajorType> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return responseReadBasic(request, ProductMajorType.class);
	}
	
	/**
	 * 响应PUT请求
	 * 修改商品一级分类
	 * 
	 * @return 操作结果
	 */
	@Override
	protected Response<ProductMajorType> responseUpdate(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		Integer id = Utility.toInteger(request.getParameter(Dictionary.PK));
		if (id == null) {
			return invalid("主键不能为空", null);
		}
		final ProductMajorType type = find(ProductMajorType.class, id);
		String column = request.getParameter(Dictionary.COLUMN);
		String value = request.getParameter(Dictionary.VALUE);
		if (type == null) {
			return invalid("无效的主键", null);
		}
		if (Utility.isEmpty(column)) {
			return invalid("修改域不能为空", null);
		}
		EntityTransaction transaction = getTransaction();
		try {
			transaction.begin();
			switch (column) {
				case Dictionary.NAME: {
					type.setName(value);
					break;
				}
				default:
					return invalid("无效的修改域", null);
			}
			if (type.isValid()) {
				transaction.commit();
				return succeed("修改成功", type);
			}
		} catch (IllegalStateException | RollbackException exception){
			return exception("执行发生异常", null);
		} finally {
			if (transaction.isActive()) {
				transaction.rollback();
			}
		}
		return failed("修改失败", null);
	}
}
