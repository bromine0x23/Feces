package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.model.ProductMajorType;
import lombok.NoArgsConstructor;

/**
 * 获取所有商品一级分类
 * 
 * @author Bromine0x23
 */
@WebServlet(name="product_major_types", urlPatterns={"/api/product_major_types"})
@NoArgsConstructor
public class ProductMajorTypesServlet extends RESTfulServlet {

	private static final long serialVersionUID = 5165925526489311969L;
	
	/**
	 * 响应GET请求
	 * 
	 * @return 所有商品一级分类
	 */
	@Override
	protected Response<List<ProductMajorType>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return succeed(findAll(ProductMajorType.class));
	}

}
