package feces.servlet;

import feces.model.*;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@WebServlet(name="substation",  urlPatterns={"/api/substation"})
@NoArgsConstructor(access=AccessLevel.PROTECTED)
public class SubstationServlet extends RESTfulServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7436774820484131481L;

	@Override
	protected Response<List<Substation>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response )throws IOException,ServletException{
			return succeed(findAll(Substation.class));
		
	}
}
