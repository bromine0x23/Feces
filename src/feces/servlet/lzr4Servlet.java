package feces.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import feces.Database;
import feces.model.TaskVoucher;

public class lzr4Servlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public lzr4Servlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html");
		System.out.println("get");
		String i=URLDecoder.decode(request.getParameter("id"),"utf-8" );
		System.out.println(i);
		int a=Integer.parseInt(i.toString().trim());
		System.out.println(a);
		HttpSession session = request.getSession();
		session.setAttribute("attr2",a);
		
		List<TaskVoucher> ltv=new ArrayList<TaskVoucher>();
		String state="已分配";
		int deliver=1;
		ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.deliver=?1 AND e.task_state=?2", TaskVoucher.class,deliver,state);
		int size1=ltv.size();
		deliver=2;
		ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.deliver=?1 AND e.task_state=?2", TaskVoucher.class,deliver,state);
		int size2=ltv.size();
		deliver=3;
		ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.deliver=?1 AND e.task_state=?2", TaskVoucher.class,deliver,state);
		int size3=ltv.size();
		
		session.setAttribute("deliver1", size1);
		session.setAttribute("deliver2", size2);
		session.setAttribute("deliver3", size3);
		
		
		response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/lzr_deliver_detail.jsp");
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
