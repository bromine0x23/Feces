package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.NoArgsConstructor;
import feces.model.Client;
import feces.model.Product;
import feces.servlet.RESTfulServlet.Response;
@WebServlet(name="clients", urlPatterns={"/api/clients"})
@NoArgsConstructor
public class ClientsServlet extends RESTfulServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5020323038783500110L;

	/**
	 * 响应GET请求
	 * 获取所有客户
	 * 
	 * @return 所有客户
	 */
	@Override
	protected Response<List<Client>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return succeed(findAll(Client.class));
	}
}
