package feces.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.Database;
import feces.model.Client;
import feces.model.ClientBackOrder;
import feces.model.ClientOrder;
import feces.model.ClientOrderList;
import lombok.NoArgsConstructor;

@WebServlet(name="back_order", urlPatterns={"/api/backorder"})
@NoArgsConstructor
public class Back_Order_Servlet extends HttpServlet{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1419174538094051780L;
	
	private static final String CONTENT_TYPE = "text/html; charset=utf-8";

	private ClientOrder cl;
	public void doGet(
		HttpServletRequest request, 
		HttpServletResponse response)
		throws ServletException, IOException 
	{
		
		//初始化对象
		response.setContentType(CONTENT_TYPE);
		String str = request.getParameter("back_info");
		String idstr=request.getParameter("id");
		String back_order=new String(str.getBytes("iso-8859-1"),"utf-8");
		String id=new String(idstr.getBytes("iso-8859-1"),"utf-8");
		if((id!=null)&&(id.isEmpty()==false))
		{
			cl = Database.find(ClientOrder.class, Integer.parseInt(id));
		}
		final ClientBackOrder cbo=new ClientBackOrder();
		cl.setType("已退货");
		cbo.setClientorder(cl);
		cbo.setOrderid(Integer.parseInt(id));
		cbo.setReason(back_order);
		if (cbo.isValid()) {
			boolean result =  Database.doTransaction(new Runnable() {
					
				@Override
				public void run() {
					Database.add(cbo);
					Database.update(cl);
					 
				}

			});
				//return true;
			///return false;
		}
		response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/ClientServer/hwz_client_info.html");
		//return false;
		
	}
}
