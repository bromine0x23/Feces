package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import feces.model.PurchaseOrderProduct;
import feces.utility.Utility;

public class PurOrderProductServlet extends RESTfulServlet {

	private static final long serialVersionUID = 2688384756384231753L;


	/**
	 * 字典
	 * 
	 * @author Bromine0x23
	 */
	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class Dictonary {
		public static final String ORDER_ID = "order_id";

		public static final String PRODUCT_ID = "product_id";
		
		public static final String PRODUCT = "product";
		
		public static final String NEED_QUANTITY = "need_quantity";
		
		public static final String REAL_QUANTITY = "real_quantity";

		
	}
	
	
	/** 响应get请求
	 * 获取购货单商品
	 * 
	 */
	@Override
	protected Response<List<PurchaseOrderProduct>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		String parameter = request.getParameter(Dictonary.ORDER_ID);
		if (parameter == null) {
			return invalid(null);
		}
		Integer id = Utility.toInteger(parameter);
		if (id == null) {
			return invalid(null);
		}
		List<PurchaseOrderProduct> list=
		executeQuery("select x from PurchaseOrderProduct where order_id=?1",PurchaseOrderProduct.class,Dictonary.ORDER_ID);
			if(list!=null)	{
			return succeed(list);
			}
		return failed(null);
	}
}
