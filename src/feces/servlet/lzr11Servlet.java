package feces.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import feces.Database;
import feces.model.ClientOrder;
import feces.model.TaskVoucher;

public class lzr11Servlet extends RESTfulServlet {

	/**
	 * Constructor of the object.
	 */
	public lzr11Servlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html");

		String id=request.getParameter("hide_id");
		int id1=Integer.parseInt(id.toString().trim());
	
		List<TaskVoucher> ltv=new ArrayList<TaskVoucher>();
		ltv = Database.executeQuery("SELECT DISTINCT e FROM TaskVoucher e WHERE e.task_number=?1", TaskVoucher.class,id1);
		
		final TaskVoucher type=ltv.get(0);
	
		type.setTask_type("退货");
		type.setTask_state("已领货");
		final ClientOrder co=type.getOrder();
		
		co.setType("退货");
	
		boolean result = doTransaction(new Runnable() {

			@Override
			public void run() {
				update(type);
				update(co);
//				TaskVoucher c=new TaskVoucher();
//				c.setTask_number(44);
//				c.setSubstation(type.getSubstation());
//				c.setOrder(type.getOrder());
//				c.setDeliver(1);
//				c.setTask_type("收款");
//				c.setTask_state("可分配");
//				c.setDate(type.getDate());
//				add(c);

				
			}

		});
		
		response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/lzr_search_final.jsp");
		
		
		
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
