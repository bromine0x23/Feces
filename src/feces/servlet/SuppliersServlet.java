package feces.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import feces.model.Supplier;
import lombok.NoArgsConstructor;

/**
 * 获取所有供应商信息
 * 
 * @author Bromine0x23
 */
@WebServlet(name = "suppliers", urlPatterns = { "/api/suppliers" })
@NoArgsConstructor
public class SuppliersServlet extends RESTfulServlet {

	private static final long serialVersionUID = 736710981414331112L;

	/**
	 * 响应GET请求 获取所有供应商
	 * 
	 * @return 所有供应商
	 */
	@Override
	protected Response<List<Supplier>> responseRead(
		HttpServletRequest request,
		HttpServletResponse response
	) throws ServletException, IOException {
		return succeed(findAll(Supplier.class));
	}

}
