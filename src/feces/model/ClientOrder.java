package feces.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * 这是一个订单的实体类
 * 提供了订单的获取，增加，删除等一系列操作
 * 并为详细订单提供基础结构
 * @author 韩文政
 * @see 5.1
 */
@Entity
@Table(schema = "SCOTT", name = "CLIENT_ORDER")
public class ClientOrder implements IValidatable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4986467849113922480L;
	@SequenceGenerator(
			name = "ORDER_SEQ",
			sequenceName = "ORDER_SEQ",
			allocationSize = 1
		)
	/**
	 * 订单编号
	 * 存储为<b>ID,Interger</b>
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORDER_SEQ")
	@Column(name = "ID", nullable=false)
	private Integer id;
	/**
	 * 订购客户
	 * 存储为<b>ORDER_ID,INTERGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "ORDER_CLIENT", nullable=false)
	private Client order_client;
	/**
	 * 收货客户
	 * 存储为<b>GET_ID,INTERGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "GET_CLIENT", nullable=false)

	private Client get_client;
	/**
	 * 投递信息
	 * 存储为<b>SENF_INFO,VARCHAR(50)</b>
	 */
	@Column(name = "SEND_INFO",length=50, nullable=false)
	private String send_info;
	/**
	 * 资金信息
	 * 存储为<b>IS_PAY,INTEGER</b>
	 */
	@Column(name="IS_PAY", nullable=false)
	private int is_pay;
	/**
	 * 货物信息
	 * 存储为<b>PRO_ID,INTERGER</b>
	 */
	@Column(name = "PRO_ID")
	private	Integer pro_id;

	/**
	 * 货物信息
	 * 存储为<b>OrderID,INTERGER</b>
	 */
	@OneToMany
	@JoinColumn(name = "ORDER_ID", referencedColumnName = "ID", nullable = false)
	private	List<ClientOrderList> products;
	/**
	 * 货物数量
	 * 存储为<b>COUNT,INTERGER</b>
	 */
	@Column(name="NUM", nullable=false)
	private Integer num;
	
	@Column(name = "CREATED", nullable=false,insertable=false)
	private Timestamp created;
	public Timestamp getCreated() {
		return created;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Date getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(java.util.Date date) {
		this.orderdate = (Date) date;
	}

	/**
	 * 创建时间
	 * 存储为<b>ORDERDATE,date</b>
	 */
	@Column(name="ORDERDATE", nullable=false)
	private Date orderdate;
	/**
	 * 计量单位
	 * 存储为<b> UNIT,VARCHAR(10)</b>
	 */
	@Column(name="UNIT",length=10)
	private String unit;
	/**
	 * 订单类型
	 * 存储为<b>TYPE,VARCHAR(10)</b>
	 */
	@Column(name="TYPE",length=10)
	private String type;
	/**
	 * 送货要求信息
	 * 存储为<b>REQ_INFO,VARCHAR(50)</b>
	 */
	@Column(name="REQ_INFO",length=50, nullable=false)
	private String req_info;
	/**
	 * 订单状态
	 * 存储为<b>STATUS,VARCHAR(20)</b>
	 */
	@Column(name="STATUS",length=20, nullable=false)
	private String status;
	/**
	 * 发票信息
	 * 存储为<b>CH_INFO,VARCHAR(50)</b>
	 */
	@Column(name="CH_INFO",length=50, nullable=false)
	private String ch_info;
	/**
	 * 备注信息
	 * 存储为<b>COMMENT,VARCHAR(50)</b>
	 */
	@Column(name="REMARK",length=50)
	private String remark;
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Client getOrder_client() {
		return order_client;
	}
	public void setOrder_client(Client order_client) {
		this.order_client = order_client;
	}
	public Client getGet_client() {
		return get_client;
	}
	public void setGet_client(Client get_client) {
		this.get_client = get_client;
	}
	public String getSend_info() {
		return send_info;
	}
	public void setSend_info(String send_info) {
		this.send_info = send_info;
	}
	public int getIs_pay() {
		return is_pay;
	}
	public void setIs_pay(int is_pay) {
		this.is_pay = is_pay;
	}
	public List<ClientOrderList> getProducts() {
		return products;
	}
	public void setProducts(List<ClientOrderList> products) {
		this.products = products;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReq_info() {
		return req_info;
	}
	public void setReq_info(String req_info) {
		this.req_info = req_info;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCh_info() {
		return ch_info;
	}
	public void setCh_info(String ch_info) {
		this.ch_info = ch_info;
	}

	public Integer getPro_id() {
		return pro_id;
	}
	public void setPro_id(Integer pro_id) {
		this.pro_id = pro_id;
	}
	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return true;
	}
}