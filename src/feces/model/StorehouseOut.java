package feces.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * 出库记录
 * 
 * @author Bromine0x23
 * @see 5.4.2
 * @see 5.5.3.2.4
 */
@Entity
@DiscriminatorValue(value = "O")
public class StorehouseOut extends StoreRecord {

	private static final long serialVersionUID = 1074897292268496010L;
	public StorehouseOut() {
		super();
		this.type = 'O';
	}
	
}
