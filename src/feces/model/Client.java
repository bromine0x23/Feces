package feces.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 这是一个客户的实体类
 * 提供了客户的数据库访问接口
 * 并为详细客户提供父类实现
 * @author 韩文政
 * @see 5.1
 */
@Entity
@Table(schema = "SCOTT", name = "CLIENTBEAN")
public class Client{
	@SequenceGenerator(
			name = "CLIENT_SEQ",
			sequenceName = "CLEANT_SEQ",
			allocationSize = 1
		)
	
	/**
	 *客户ID
	 *主键、存储为<b>ID: INTEGER</b>
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLIENT_SEQ")
	@Column(name = "ID")
	private Integer id;
	/**
	 * 客户名称
	 * 非空、存储为<b>NAME: NVARCHAR2(40)</b>
	 */
	@Column(name = "NAME",nullable=false, length = 40)
	private String name=null;
	/**
	 * 单位名称
	 * 存储为<b>BU:VARCHAR2(30)</b>
	 */
	@Column(name="BU",length = 40)
	private String bu;
	/**
	 * 固定电话
	 * 存储为<b>PHONE:VARCHAR(30)</b>
	 */
	@Column(name="PHONE",length=40)
	private String phone;
	/**
	 * 移动电话
	 * 存储为<b>MPHONE:VARCHAR(20)</b>
	 */
	@Column(name="MPHONE",nullable=false, length=40)
	private String mphone;
	/**
	 * 身份证号
	 * 存储为<b>ID_CARD:VARCHAR(18)
	 */
	@Column(name="ID_CARD",nullable=false, length=18)
	private String id_card;
	/**
	 * 联系地址
	 * 存储为<b>ADDRESS:VARCHAR(40)
	 */
	@Column(name="ADDRESS",nullable=false, length=40)
	private String address;
	/**
	 * 邮编
	 * 存储为<b>MAIL:VARCHAR(6)</b>
	 */
	@Column(name="MAIL",length=6)
	private String mail;       //邮编
	/**
	 * 电子邮箱
	 * 存储为<b>EMAIL:VARCHAR(40)</b>
	 */
	private String email;
	/**
	 * 构造函数，限制必须项必须输入
	 * 参数列表：客户姓名、身份证号码、
	 * 移动电话号码、通讯地址
	 */
	public Client(String name,//姓名
			String idcard,//身份证号码
			String mphone,//移动电话号码
			String address)//通讯地址
	{
		this.name=name;
		this.id_card=idcard;
		this.mphone=mphone;
		this.address=address;
	}
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getBu() {
		return bu;
	}


	public void setBu(String bu) {
		this.bu = bu;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getMphone() {
		return mphone;
	}


	public void setMphone(String mphone) {
		this.mphone = mphone;
	}


	public String getId_card() {
		return id_card;
	}


	public void setId_card(String id_card) {
		this.id_card = id_card;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	
	public Client() {
		// do nothing
	}
	
	/*
	 * 判断各项属性是否正常(non-Javadoc)
	 * @see feces.model.Model#isValid()
	 */

	public boolean isValid() {
		// TODO 判断是否合法
		return true;
	}
}
