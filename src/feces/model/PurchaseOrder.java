package feces.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Column;


/**
 * 购货单，存储在表<b>PURCHASE_ORDER</b>
 * @author 李则宇
 * 5.4.1
 */
@Entity
@Table(schema = "SCOTT", name = "PURCHASE_ORDER")
public class PurchaseOrder implements IValidatable{

	private static final long serialVersionUID = 8480576203175615360L;

	@SequenceGenerator(
			name = "PURCHASE_ORDER_ID",
			sequenceName = "PURCHASE_ORDER_ID",
			allocationSize = 1
	)
	
	
	/**
	 * 购货单号
	 * 主键，非空，存储为 ID：INTEGER
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PURCHASE_ORDER_ID")
	@Column(name="ID",nullable=false)
	private Integer id;
	
	@OneToMany
	@JoinColumn(name = "ORDER_ID", referencedColumnName = "ID", nullable = false)
	private	List<PurchaseOrderProduct> products;
	
	/**
	 * 购货单创建日期
	 * 存储为 CREATION_DATE：DATE
	 */
	@Column(name="CREATION_DATE")
	private Date creation_date;
	
	/**
	 * 此单是否完成
	 * 存储为 IS_FINISHED：NUMBER(1)
	 */
	@Column(name="IS_FINISHED")
	private Integer is_finished; 
		
	
	public Integer getId() {
		return id;
	}

	public List<PurchaseOrderProduct> getProducts() {
		return products;
	}
	
	public PurchaseOrder addProduct(PurchaseOrderProduct product) {
		products.add(product);
		return this;
	}

	public PurchaseOrder removeProduct(PurchaseOrderProduct product) {
		products.remove(product);
		return this;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}

	public Integer getIs_finished() {
		return is_finished;
	}

	public void setIs_finished(Integer is_finished) {
		this.is_finished = is_finished;
	}

	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return true;
	}

	public void setProducts(List<PurchaseOrderProduct> products) {
		this.products = products;
	}

	
}