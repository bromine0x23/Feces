package feces.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Column;

import feces.validator.StringValidator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 供应商(Supplier)
 *
 * @author Bromine0x23
 * @see 5.5.2.2.1
 */
@Entity
@Table(schema = "SCOTT", name = "SUPPLIER")
@NoArgsConstructor
public class Supplier implements IValidatable {

	private static final long serialVersionUID = 1488146665677564469L;

	/**
	 * 供应商代码，主键、存储为<b>ID: INTEGER</b>
	 */
	@Id
	@SequenceGenerator(name = "SUPPLIER_ID", sequenceName = "SUPPLIER_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SUPPLIER_ID")
	@Column(name = "ID", nullable = false)
	@Getter
	private Integer id;

	/**
	 * 供应商名称，非空、存储为<b>NAME: NVARCHAR2(40)</b>
	 */
	@Column(name = "NAME", nullable = false, length = 40)
	@Getter
	@Setter
	private String name;

	/**
	 * 地址，非空、存储为<b>ADDRESS: NVARCHAR2(100)</b>
	 */
	@Column(name = "ADDRESS", nullable = false, length = 100)
	@Getter
	@Setter
	private String address;

	/**
	 * 联系人，非空、存储为<b>CONTACT: NVARCHAR2(20)</b>
	 */
	@Column(name = "CONTACT", nullable = false, length = 20)
	@Getter
	@Setter
	private String contact;

	/**
	 * 联系电话，非空、存储为<b>CONTACT_PHONE: VARCHAR2(20)</b>
	 */
	@Column(name = "CONTACT_PHONE", nullable = false, length = 20)
	@Getter
	@Setter
	private String contactPhone;

	/**
	 * 开户行，非空、存储为<b>BANK: NVARCHAR2(40)</b>
	 */
	@Column(name = "BANK", nullable = false, length = 40)
	@Getter
	@Setter
	private String bank;

	/**
	 * 银行账户，非空、存储为<b>BANK_ACCOUNT: VARCHAR2(20)</b>
	 */
	@Column(name = "BANK_ACCOUNT", nullable = false, length = 20)
	@Getter
	@Setter
	private String bankAccount;

	/**
	 * 传真，存储为<b>FAX: VARCHAR2(20)</b>
	 */
	@Column(name = "FAX", length = 20)
	@Getter
	@Setter
	private String fax;

	/**
	 * 邮编，存储为<b>ZIP: VARCHAR2(10)</b>
	 */
	@Column(name = "ZIP", length = 10)
	@Getter
	@Setter
	private String zip;

	/**
	 * 法人，非空、存储为<b>CORPORATION: NVARCHAR2(20)</b>
	 */
	@Column(name = "CORPORATION", nullable = false, length = 20)
	@Getter
	@Setter
	private String corporation;

	/**
	 * 备注，存储为<b>REMARK: NVARCHAR2(200)</b>
	 */
	@Column(name = "REMARK", length = 200)
	@Getter
	@Setter
	private String remark;

	@Override
	public boolean isValid() {
		return StringValidator.validate(getName(), 40) && StringValidator.validate(getAddress(), 100)
				&& StringValidator.validate(getContact(), 20) && StringValidator.validate(getContactPhone(), 20)
				&& StringValidator.validate(getBank(), 40) && StringValidator.validate(getBankAccount(), 20)
				&& StringValidator.validate(getFax(), 20, true) && StringValidator.validate(getZip(), 10, true)
				&& StringValidator.validate(getCorporation(), 20, true)
				&& StringValidator.validate(getRemark(), 200, true);
	}
}
