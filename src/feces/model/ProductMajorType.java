package feces.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import feces.validator.StringValidator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品一级分类(Product Major Type)
 * 
 * @author Bromine0x23
 * @see 5.5.1.2.1
 */
@Entity
@Table(schema = "SCOTT", name = "PRODUCT_MAJOR_TYPE")
@NoArgsConstructor
public class ProductMajorType implements IValidatable {

	private static final long serialVersionUID = 9145414268283970593L;

	/**
	 * 分类代码，主键、存储为<b>ID: INTEGER</b>
	 */
	@Id
	@SequenceGenerator(name = "PRODUCT_MAJOR_TYPE_ID", sequenceName = "PRODUCT_MAJOR_TYPE_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCT_MAJOR_TYPE_ID")
	@Column(name = "ID", nullable = false)
	@Getter
	private Integer id;

	/**
	 * 分类名称，非空、存储为<b>NAME: NVARCHAR2(40)</b>
	 */
	@Column(name = "NAME", nullable = false, length = 40)
	@Getter
	@Setter
	private String name;

	@Override
	public boolean isValid() {
		return StringValidator.validate(getName(), 40);
	}
}
