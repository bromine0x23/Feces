package feces.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 出库单，存储在表<b>OUTBOUND_ORDER</b>
 * @author 李则宇
 * @author Bromine0x23
 * 5.4.2.2.2
 */
@Entity
@Table(schema = "SCOTT", name = "OUTBOUND_ORDER")
@Deprecated
public class OutboundOrder implements IValidatable {
	
	private static final long serialVersionUID = 1507164409736729316L;

	@SequenceGenerator(
		name = "OUTBOUND_ORDER_ID",
		sequenceName = "OUTBOUND_ORDER_ID",
		allocationSize = 1
	)

	/**
	 * 出库单号
	 * 主键，非空，存储为ID：INTEGER
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OUTBOUND_ORDER_ID")
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	@OneToMany
	@JoinColumn(name = "ORDER_ID", referencedColumnName = "ID", nullable = false)
	@OrderBy
	private	List<OutboundOrderProduct> products;
	
	/**
	 * 出库日期
	 * 非空，存储为OUTBOUND_DATE
	 */
	@Column(name = "OUTBOUND_DATE",nullable = false)
	private Date outbound_date;
	
	/**
	 * 备注
	 * 存储为 REMARKS
	 */
	@Column(name = "REMARK",nullable = false)
	private String remarks;

	public Integer getId() {
		return id;
	}

	public List<OutboundOrderProduct> getProducts() {
		return products;
	}
	
	public OutboundOrder addProduct(OutboundOrderProduct product) {
		products.add(product);
		return this;
	}

	public OutboundOrder removeProduct(OutboundOrderProduct product) {
		products.remove(product);
		return this;
	}

	public Date getOutboundDate() {
		return outbound_date;
	}

	public void setOutboundDate(Date outbound_date) {
		this.outbound_date = outbound_date;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public boolean isValid() {
		// TODO 校验
		return false;
	}
	
	
}
