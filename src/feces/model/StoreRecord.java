package feces.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import feces.validator.NotNullValidator;
import feces.validator.StringValidator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 库房记录（入库 or 出库）
 * 
 * @author Bromine0x23
 */
@Entity
@NoArgsConstructor
@Table(schema = "SCOTT", name = "STORE_RECORD")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.CHAR)
public abstract class StoreRecord implements IValidatable {

	private static final long serialVersionUID = 5848610306147223158L;
	
	/**
	 * 记录流水号
	 * 主键、存储于<b>ID: INTEGER</b>
	 */
	@Id
	@SequenceGenerator(name = "STORE_RECORD_ID", sequenceName = "STORE_RECORD_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STORE_RECORD_ID")
	@Column(name = "ID", nullable = false)
	@Getter
	private Integer id;
	
	/**
	 * 库房
	 * 外键、非空、存储于<b>STOREHOUSE_ID: INTEGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "STOREHOUSE_ID", nullable = false)
	@Getter
	@Setter
	private Storehouse storehouse;
	
	/**
	 * 商品
	 * 外键、非空、存储于<b>PRODUCT_ID: INTEGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "PRODUCT_ID", nullable = false)
	@Getter
	@Setter
	private Product product;
	
	/**
	 * 商品数量
	 * 非空、存储于<b>AMOUNT: INTEGER</b>
	 */
	@Column(name = "AMOUNT", nullable = false)
	@Getter
	@Setter
	private Integer amount;
	
	/**
	 * 任务
	 * 外键、存储于<b>TASK_ID: INTEGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "TASK_ID", nullable = true)
	@Getter
	@Setter
	private TaskVoucher task;

	/**
	 * 备注
	 * 存储为 REMARKS
	 */
	@Column(name = "REMARK", nullable = true)
	@Getter
	@Setter
	private String remark;
	
	/**
	 * 
	 */
	@Column(name = "TYPE", nullable = false)
	@Getter
	protected Character type;
	
	/**
	 * 建立日期
	 * 外键、非空、存储于<b>CREATED: TIMESTAMP DEFAULT SYSDATE</b>
	 */
	@Column(name = "CREATED", insertable = false, nullable = true)
	@Getter
	private Timestamp created;
	
	@Override
	public boolean isValid() {
		return NotNullValidator.validate(getStorehouse())
			&& NotNullValidator.validate(getProduct())
			&& getAmount() >= 0
			&& NotNullValidator.validate(getTask())
			&& StringValidator.validate(getRemark(), 200, true);
	}

}
