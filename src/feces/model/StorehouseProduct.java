package feces.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import feces.validator.NotNullValidator;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 库房储备(Storehouse Product)
 * @author Bromine0x23
 * @see 5.5.3.2
 */
@Entity
@IdClass(StorehouseProduct.PrimaryKey.class)
@Table(schema = "SCOTT", name = "STOREHOUSE_PRODUCT")
@NoArgsConstructor
public class StorehouseProduct implements IValidatable {
	
	
	@EqualsAndHashCode
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PrimaryKey implements Serializable {

		private static final long serialVersionUID = -6801799894575835260L;

		@Getter
		private Integer storehouseId;
		
		@Getter
		private Integer productId;
	}

	private static final long serialVersionUID = 2354971274728158823L;

	/**
	 * 库房代码
	 * 主键、外键、存储为<b>STOREHOUSE_ID: INTEGER</b>
	 */
	@Id
	@Column(name = "STOREHOUSE_ID", nullable = false)
	@Getter
	@Setter
	private Integer storehouseId;
	
	/**
	 * 商品代码
	 * 主键、外键、存储为<b>PRODUCT_ID: INTEGER</b>
	 */
	@Id
	@Column(name = "PRODUCT_ID", nullable = false)
	@Getter
	@Setter
	private Integer productId;
	
	/**
	 * 当前库存
	 * 非空、存储为<b>QUANTITY: INTEGER</b>
	 */
	@Column(name = "QUANTITY", nullable = false)
	@Getter
	@Setter
	private Integer quantity;
	
	/**
	 * 警戒库存
	 * 非空、存储为<b>CRITICAL: INTEGER</b>
	 */
	@Column(name = "CRITICAL", nullable = false)
	@Getter
	@Setter
	private Integer critical;
	
	/**
	 * 最大库存
	 * 非空、存储为<b>LIMIT: INTEGER</b>
	 */
	@Column(name = "LIMIT", nullable = false)
	@Getter
	@Setter
	private Integer limit;

	@Override
	public boolean isValid() {
		return NotNullValidator.validate(getStorehouseId())
			&& NotNullValidator.validate(getProductId())
			&& NotNullValidator.validate(getQuantity())
			&& NotNullValidator.validate(getCritical())
			&& NotNullValidator.validate(getLimit());
	}

}
