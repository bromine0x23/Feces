package feces.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@IdClass(ClientOrderList.PrimaryKey.class)
@Table(schema = "SCOTT", name = "CLIENT_ORDER_LIST")
public class ClientOrderList implements IValidatable {
/**
	 * 
	 */
private static final long serialVersionUID = 4762334597676199282L;
public static class PrimaryKey implements Serializable{
		
		private static final long serialVersionUID = -6801799894575835260L;

		private Integer order_id;
		
		private Integer product_id;

		public Integer getOrder_id() {
			return order_id;
		}

		public Integer getProduct_id() {
			return product_id;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((order_id == null) ? 0 : order_id.hashCode());
			result = prime * result
					+ ((product_id == null) ? 0 : product_id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PrimaryKey other = (PrimaryKey) obj;
			if (order_id == null) {
				if (other.order_id != null)
					return false;
			} else if (!order_id.equals(other.order_id))
				return false;
			if (product_id == null) {
				if (other.product_id != null)
					return false;
			} else if (!product_id.equals(other.product_id))
				return false;
			return true;
		}
	}
	
	@Id
	@Column(name = "ORDER_ID", nullable = false)
	private Integer order_id;
	
	@Id
	@Column(name = "PRODUCT_ID", nullable = false)
	private Integer product_id;

	@OneToOne
	@JoinColumn(name = "PRODUCT_ID", nullable = false, updatable = false,insertable = false)
	private Product product;
	
	/**
	 * 商品数量
	 * 非空，存储为PRO_NUMBER:INTEGER
	 */
	@Column(name="PRO_NUMBER",nullable=false)
	private	Integer product_number;

	public Integer getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}


	public Integer getProduct_number() {
		return product_number;
	}

	public void setProduct_number(Integer product_number) {
		this.product_number = product_number;
	}

	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return true;
	}

}
