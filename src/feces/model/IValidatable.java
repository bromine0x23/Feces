package feces.model;

import java.io.Serializable;

/**
 * 具有{@link IValidatable#isValid()}方法
 * @author Bromine0x23
 */
public interface IValidatable extends Serializable {

	/**
	 * 验证实体合法性
	 * @return 合法性
	 */
	public abstract boolean isValid();
}
