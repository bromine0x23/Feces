/**
 * 
 */
package feces.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.ws.rs.GET;

import lombok.Getter;
import lombok.Setter;

/**
 * @author new
 *
 */

@Entity
@Table(schema="SCOTT",name="DETAILED_ORDER")
public class DetailedOrder implements IValidatable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7869261862588181121L;


	@Id
	@SequenceGenerator(
		name="DETAILED_ORDER_ID",
		sequenceName="DETAILED_ORDER_ID",
		allocationSize=1
			)
	
	
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DETAILED_ORDER_ID")
	@Column(name="ID", nullable=false)
	@Getter
	private Integer id;
	
	
	@Column(name="TYPE",nullable=false)
	@Getter
	@Setter
	private String type;


	@Column(name="STATUS",nullable=false)
	@Getter
	@Setter
	private String status;
	

	@Column(name="CLIENT",nullable=false)
	@Getter
	@Setter
	private String client;
	
	@Column(name="DATE")
	@Getter
	@Setter
	private Date date;
	
	
	@Column(name= "PNAME")
	@Getter
	@Setter
	private String pname;
	
	@Column(name="NUM")
	@Getter
	@Setter
	private Integer num;
	
	@Column(name="SUM")
	@Getter
	@Setter
	private Double sum;
	
	
	@Column(name="RECEIVER")
	@Getter
	@Setter
	private String receiverString;
	
	@Column(name="ADDRESS")
	@Getter
	@Setter
	private String address;
	
	@Column(name="PHONE")
	@Getter
	@Setter
	private String phone;
	
	@Column(name="MAIL")
	@Getter
	@Setter
	private String mail;
	
	@Column(name="INVOICE")
	@Getter
	@Setter
	private String invoice;
	
	@Column(name="REMARK")
	@Getter
	@Setter
	private String remark;
	
	
	
	public DetailedOrder() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return true;
	}

}
