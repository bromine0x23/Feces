package feces.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * 这是回执1号单的实体类，存储在表<b>BACK1</b>
 * 
 * @author 刘卓瑞
 * @5.3
 * 
 * 
 */
 @Entity
 @Table(schema = "SCOTT", name = "BACK")

public class Back1 {
	 @SequenceGenerator(name = "BILL", sequenceName = "BILL", allocationSize = 1)
		
		/**
		 * 回执单号
		 * 主键，非空，存储为ID：INTEGER
		*/
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BILL")
	@Column(name = "BILL")
	private int bill;
	 /**
	  * 外键：任务单；一个反馈单对应一个任务单
	  */
		
	@OneToOne
	@JoinColumn(name = "TASKNUMBER", referencedColumnName = "TASKNUMBER")
	private TaskVoucher taskvoucher;
	 
	/**
	 * 客户满意度
	 */
	@Column(name = "SATISFACATION", nullable = true)
	private int satisfaction;
	/**
	 * 备注信息
	 */
	@Column(name = "REMARK", length = 50, nullable = true)
	private String remark;
	public int getBill() {
		return bill;
	}
	public void setBill(int bill) {
		this.bill = bill;
	}
	public int getSatisfaction() {
		return satisfaction;
	}
	public void setSatisfaction(int satisfaction) {
		this.satisfaction = satisfaction;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public TaskVoucher getTaskvoucher() {
		return taskvoucher;
	}
	public void setTaskvoucher(TaskVoucher taskvoucher) {
		this.taskvoucher = taskvoucher;
	}
	
	
}
