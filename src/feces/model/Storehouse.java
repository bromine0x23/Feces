package feces.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import feces.validator.NotNullValidator;
import feces.validator.StringValidator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 库房(Storehouse)
 * 
 * @author Bromine0x23
 * @see 5.5.3.2.1
 */
@Entity
@Table(schema = "SCOTT", name = "STOREHOUSE")
@NamedQueries({
	@NamedQuery(
		name = "Storehouse.findType",
		query = "SELECT DISTINCT s FROM Storehouse s WHERE s.type = ?1"
	)
})
@NoArgsConstructor
public class Storehouse implements IValidatable {

	private static final long serialVersionUID = -2160760133259169338L;

	public static enum Type {
		MAIN, SUB
	}

	/**
	 * 库房代码，主键，存储为<b>ID: INTEGER</b>，使用序列<b>STOREHOUSE_ID</b>
	 */
	@Id
	@SequenceGenerator(name = "STOREHOUSE_ID", sequenceName = "STOREHOUSE_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STOREHOUSE_ID")
	@Column(name = "ID", nullable = false)
	@Getter
	private Integer id;

	/**
	 * 库房名称，非空，存储为<b>NAME: NVARCHAR2(40)</b>
	 */
	@Column(name = "NAME", nullable = false, length = 40)
	@Getter
	@Setter
	private String name;

	/**
	 * 库房地址，非空，存储为<b>NAME: NVARCHAR2(100)</b>
	 */
	@Column(name = "ADDRESS", nullable = false, length = 100)
	@Getter
	@Setter
	private String address;

	/**
	 * 库管员，非空，存储为<b>NAME: NVARCHAR2(20)</b>
	 */
	@Column(name = "MANAGER", nullable = false, length = 20)
	@Getter
	@Setter
	private String manager;

	/**
	 * 库房类别，非空，存储为<b>TYPE: NVARCHAR2(5)</b>
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE", nullable = false, length = 5)
	@Getter
	@Setter
	private Type type;

	@Override
	public boolean isValid() {
		return StringValidator.validate(getName(), 40)
			&& StringValidator.validate(getAddress(), 100)
			&& StringValidator.validate(getManager(), 20)
			&& NotNullValidator.validate(type);
	}
}
