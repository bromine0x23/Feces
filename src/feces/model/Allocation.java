package feces.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * 商品调拨单(Allocation)
 * @author 韩少华
 */
@Entity
@Table(schema = "SCOTT", name = "ALLOCATION")
public class Allocation implements IValidatable {
	


	/**
	 * 
	 */
	private static final long serialVersionUID = -8833794605924387522L;

	@SequenceGenerator(
		name = "ALLOCATION_ID",
		sequenceName = "ALLOCATION_ID",
		allocationSize = 1
	)
	
	/**
	 * 调拨单号
	 * 主键、存储为<b>ID: INTEGER</b>
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ALLOCATION_ID")
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	/**
	 * 订单号
	 * 外键、非空、存储为<b>CLIENT_ORDER_ID: INTEGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "CLIENT_ORDER_ID", nullable = false)
	private ClientOrder clientOrder;
	/**
	 * 任务单号
	 * 外键、非空、存储为<b>TASKVOUCHER_ID: INTEGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "TASKVOUCHER_ID", nullable = false)
	private TaskVoucher taskVoucher;

	/**
	 * 出库库房
	 * 外键、非空、存储为<b>OUT_STOREHOUSE_ID: INTEGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "OUT_STOREHOUSE_ID", nullable = false)
	private Storehouse outStorehouse;
	
	/**
	 * 入库库房
	 * 外键、非空、存储为<b>IN_STOREHOUSE_ID: INTEGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "IN_STOREHOUSE_ID", nullable = false)
	private Storehouse inStorehouse;
	
	/**
	 * 要求出库日期
	 * 非空、存储为<b>OUT_DATE: DATE</b>
	 */
	@Column(name = "OUT_DATE", nullable = false)
	private Date out_date;
	
	public Allocation() {
		super();
	}

	
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public ClientOrder getClientOrder() {
		return clientOrder;
	}


	public void setClientOrder(ClientOrder clientOrder) {
		this.clientOrder = clientOrder;
	}


	public TaskVoucher getTaskVoucher() {
		return taskVoucher;
	}


	public void setTaskVoucher(TaskVoucher taskVoucher) {
		this.taskVoucher = taskVoucher;
	}



	
	
	public Storehouse getOutStorehouse() {
		return outStorehouse;
	}


	public void setOutStorehouse(Storehouse outStorehouse) {
		this.outStorehouse = outStorehouse;
	}


	public Storehouse getInStorehouse() {
		return inStorehouse;
	}


	public void setInStorehouse(Storehouse inStorehouse) {
		this.inStorehouse = inStorehouse;
	}

	
	public Date getOut_date() {
		return out_date;
	}


	public void setOut_date(Date out_date) {
		this.out_date = out_date;
	}


	@Override
	public boolean isValid() {
		return false;
	}

}
