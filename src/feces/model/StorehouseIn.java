package feces.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * 入库记录
 * 
 * @author Bromine0x23
 * @see 5.4.3
 * @see 5.5.3.2.4
 */
@Entity
@DiscriminatorValue(value = "I")
public class StorehouseIn extends StoreRecord {

	private static final long serialVersionUID = 5424784357482511156L;

	public StorehouseIn() {
		super();
		this.type = 'I';
	}
	
}
