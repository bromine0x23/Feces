package feces.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.NoArgsConstructor;

@Entity
@Table(schema = "SCOTT", name = "BACKORDER")
@NoArgsConstructor
public class ClientBackOrder implements IValidatable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5894568543593894575L;
	/**
	 * 
	 * 
	 */
	
	@Id
	@Column(name = "ORDERID", nullable=false)
	private Integer orderid;
	@OneToOne
	@JoinColumn(name = "ORDERID", nullable=false,updatable = false,insertable = false)
	private ClientOrder clientorder;
	
	
	@Column(name = "REASON", nullable=false)
	private String reason;
	
	@Column(name = "CREATED", nullable=false,insertable=false)
	private Timestamp created;
	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return true;
	}
	public Integer getOrderid() {
		return orderid;
	}
	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}
	public ClientOrder getClientorder() {
		return clientorder;
	}
	public void setClientorder(ClientOrder clientorder) {
		this.clientorder = clientorder;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Timestamp getCreated() {
		return created;
	}

}
