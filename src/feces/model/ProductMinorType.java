package feces.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import feces.validator.NotNullValidator;
import feces.validator.StringValidator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品二级分类(Product Minor Type)
 * 
 * @author Bromine0x23
 * @see 5.5.1.2.2
 */
@Entity
@Table(schema = "SCOTT", name = "PRODUCT_MINOR_TYPE")
@NamedQuery(
	name = "ProductMinorType.findByMajor",
	query = "SELECT DISTINCT t FROM ProductMinorType t WHERE t.majorType = ?1"
)
@NoArgsConstructor
public class ProductMinorType implements IValidatable {

	private static final long serialVersionUID = -8251175740614458206L;

	/**
	 * 分类代码，主键、存储为<b>ID: INTEGER</b>
	 */
	@Id
	@SequenceGenerator(name = "PRODUCT_MINOR_TYPE_ID", sequenceName = "PRODUCT_MINOR_TYPE_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCT_MINOR_TYPE_ID")
	@Column(name = "ID", nullable = false)
	@Getter
	private Integer id;

	/**
	 * 分类名称，非空、存储为<b>NAME: NVARCHAR2(40)</b>
	 */
	@Column(name = "NAME", nullable = false, length = 40)
	@Getter
	@Setter
	private String name;

	/**
	 * 父分类，外键、非空、存储于<b>MAJOR_TYPE_ID: INTEGER</b>
	 */
	@OneToOne
	@JoinColumn(name = "MAJOR_TYPE_ID", nullable = false)
	@Getter
	@Setter
	private ProductMajorType majorType;

	@Override
	public boolean isValid() {
		return StringValidator.validate(getName(), 40) && NotNullValidator.validate(getMajorType());
	}

}
