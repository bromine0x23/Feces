package feces.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 这是任务单的实体类，存储在表<b>TASKTABLE</b>
 * 
 * @author 刘卓瑞
 * @5.3
 * 
 * 
 */
@Entity
@Table(schema = "SCOTT", name = "TASKTABLE")
// @Inheritance(strategy = InheritanceType.JOINED)
public class TaskVoucher {

	@SequenceGenerator(name = "TASKNUMBER", sequenceName = "TASKNUMBER", allocationSize = 1)
	
	/**
	 * 出库单号
	 * 主键，非空，存储为ID：INTEGER
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TASKNUMBER")
	@Column(name = "TASKNUMBER")
	private int task_number;
	
	/**
	 * 外键：分站代号；用于区分是哪个分站做了该任务 非空
	 */
	
	@OneToOne
	@JoinColumn(name = "SUBSTATIONID", referencedColumnName = "SUBSTATION_ID")
	private Substation substation;
	
	/**
	 * 配送员代号 可为空，可由分站再来指陪派送员，存储为<b>DELIVER:INTEGER</b>
	 */
	@Column(name = "DELIVER", nullable = true)
	private int deliver;
	
	/**
	 * 外键，和配送的产品的订单相关联
	 * 
	 */
	@OneToOne
	@JoinColumn(name = "ORDER_ID")
	private ClientOrder order;
	
	/**
	 * 配送必须在几号前完成 非空，存储为<b>DATE:DATE</b>
	 */
	@Column(name = "TBDATE", nullable = false)
	private Date date;
	
	/**
	 * 任务类型（有收款，送货收款，送货，退货，换货等5种状态） 非空，存储为<b>TASKTYPE:VARCHAR2(30)</b>
	 */
	@Column(name = "TASKTYPE", length = 30, nullable = false)
	private String task_type;
	
	/**
	 * 任务状态（有调度，可分配，已分配，已领货，完成，失败等6种状态） 非空，存储为<b>TASKSTATE:VARCHAR2(30)</b>
	 */
	@Column(name = "TASKSTATE", length = 30, nullable = false)
	private String task_state;
	

	
	
	
	

	public int getTask_number() {
		return task_number;
	}

	public void setTask_number(int task_number) {
		this.task_number = task_number;
	}



	public int getDeliver() {
		return deliver;
	}

	public void setDeliver(int deliver) {
		this.deliver = deliver;
	}

	

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTask_type() {
		return task_type;
	}

	public void setTask_type(String task_type) {
		this.task_type = task_type;
	}

	public String getTask_state() {
		return task_state;
	}

	public void setTask_state(String task_state) {
		this.task_state = task_state;
	}



	public Substation getSubstation() {
		return substation;
	}

	public void setSubstation(Substation substation) {
		this.substation = substation;
	}

	public ClientOrder getOrder() {
		return order;
	}

	public void setOrder(ClientOrder order) {
		this.order = order;
	}


}
