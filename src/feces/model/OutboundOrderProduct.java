package feces.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 5.4.2.2.2
 */
@Entity
@IdClass(OutboundOrderProduct.PrimaryKey.class)
@Table(schema = "SCOTT", name = "OUTBOUND_ORDER_PRODUCT")
@Deprecated
public class OutboundOrderProduct implements IValidatable {

	private static final long serialVersionUID = 1603086302292635777L;

	public static class PrimaryKey implements Serializable {

		private static final long serialVersionUID = -6801799894575835260L;

		private Integer order_id;
		
		private Integer product_id;

		public Integer getOrderId() {
			return order_id;
		}
		
		public Integer getProductId() {
			return product_id;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((order_id == null) ? 0 : order_id.hashCode());
			result = prime * result + ((product_id == null) ? 0 : product_id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PrimaryKey other = (PrimaryKey) obj;
			if (order_id == null) {
				if (other.order_id != null)
					return false;
			} else if (!order_id.equals(other.order_id))
				return false;
			if (product_id == null) {
				if (other.product_id != null)
					return false;
			} else if (!product_id.equals(other.product_id))
				return false;
			return true;
		}
	}
	
	@Id
	@Column(name = "ORDER_ID", nullable = false)
	private Integer order_id;
	
	@Id
	@Column(name = "PRODUCT_ID", nullable = false)
	private Integer product_id;

	@OneToOne
	@JoinColumn(name = "PRODUCT_ID", nullable = false, updatable = false,insertable = false)
	private Product product;
	
	@Column(name = "AMOUNT", nullable = false)
	private Integer amount;
	
	@Column(name = "STOREHOUSE_ID", nullable = false)
	private Integer storehouse_id;

	public Integer getOrderId() {
		return order_id;
	}

	public void setOrderId(Integer order_id) {
		this.order_id = order_id;
	}
	
	public Integer getProductId() {
		return product_id;
	}

	public void setProductId(Integer product_id) {
		this.product_id = product_id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getStorehouseId() {
		return storehouse_id;
	}

	public void setStorehouseId(Integer storehouse_id) {
		this.storehouse_id = storehouse_id;
	}

	@Override
	public boolean isValid() {
		return false;
	}

}
