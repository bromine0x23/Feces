package feces.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(schema = "SCOTT", name = "TUSER")
@NoArgsConstructor
public class User implements Serializable {

	private static final long serialVersionUID = -6506500051413517866L;

	@Id
	@Column(name = "ID", nullable = false)
	@Getter
	private String id;
	
	@Column(name = "PASSWORD", nullable = false)
	@Getter
	private String password;
}
