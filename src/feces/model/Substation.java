package feces.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 这是分站的实体类，存储在表<b>STATIONTABLE</b>
 * @author 刘卓瑞
 *
 */
@Entity
@Table(schema = "SCOTT", name = "STATIONTABLE")

public class Substation {
	@SequenceGenerator(
		name = "SUBSTATIONID",
		sequenceName = "SUBSTATIONID",
		allocationSize = 1
	)
	/**
	* 分站编号
	* 主键，非空，存储为ID：INTEGER
	*/
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SUBSTATIONID")
	@Column(name="SUBSTATION_ID")
	int substation_id;
	/**
	 * 分站名字
	 */
	@Column(name="SUBSTATIONNAME",nullable=false)
	String substation_name;
	
	/**
	 * 分站库房
	 */
	
	@Column(name="STOREHOUSE_ID",nullable=false)
	int storehouse_id;
	
	public int getSubstation_id() {
		return substation_id;
	}
	public void setSubstation_id(int substation_id) {
		this.substation_id = substation_id;
	}
	public String getSubstation_name() {
		return substation_name;
	}
	public void setSubstation_name(String substation_name) {
		this.substation_name = substation_name;
	}
	public int getStorehouse_id() {
		return storehouse_id;
	}
	public void setStorehouse_id(int storehouse_id) {
		this.storehouse_id = storehouse_id;
	}
	
	
}
