package feces.formater;

import java.io.Writer;

import feces.utility.JSON;
import lombok.NoArgsConstructor;

/**
 * JSON格式化器
 * @author Bromine0x23
 */
@NoArgsConstructor
public class JSONFormater implements IFormater {
	
	@Override
	public String contentType() {
		return "text/json";
	}

	@Override
	public <T> String format(T object) {
		return JSON.to(object);
	}
	
	@Override
	public <T> void format(T object, Writer writer) {
		JSON.to(object, writer);
	}
}
