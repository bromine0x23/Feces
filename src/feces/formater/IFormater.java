package feces.formater;

import java.io.Writer;

/**
 * 格式化器接口
 *
 * @author Bromine0x23
 */
public interface IFormater {
	
	public static final String KEY_FORMAT = "format";
	
	/**
	 * 获得Content-Type值
	 *
	 * @return MIME类型
	 */
	public String contentType();
	
	/**
	 * 格式化对象为字符串
	 *
	 * @param object 被格式化的对象
	 * @return 格式化结果
	 */
	public <T> String format(T object);

	/**
	 * 格式化对象为字符串
	 *
	 * @param object 被格式化的对象
	 * @return 格式化结果
	 */
	public <T> void format(T object, Writer writer);
	
}
