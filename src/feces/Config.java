package feces;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 公用配置数据
 * 
 * @author Bromine0x23
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Config {

	/**
	 * 应用名
	 */
	public static final String NAME = "Feces";
	
	/**
	 * 编码
	 */
	public static final String ENCODING = "UTF-8";
}
